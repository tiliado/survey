======
Survey
======

.. image:: https://gitlab.com/tiliado/survey/badges/master/pipeline.svg
   :target: https://gitlab.com/tiliado/survey/-/pipelines

.. image:: https://gitlab.com/tiliado/survey/badges/master/coverage.svg
   :target: https://gitlab.com/tiliado/survey/-/pipelines

.. image:: https://img.shields.io/badge/license-BSD--2--Clause-blue
   :target: ./LICENSE

.. image:: https://img.shields.io/badge/code%20style-black-000000.svg
   :target: https://github.com/psf/black

.. image:: https://img.shields.io/badge/Containers-Podman-green.svg
   :target: https://podman.io

Local Development
=================

Configuration:

* Local project configuration is in the `local_cfg directory <./local_cfg/>`_.
* Unit tests configuration is in the `survey/tests/settings.py file <./survey/tests/settings.py>`_.

Requirements:

* Python 3.9 with ``pip`` and ``venv`` modules
* Bash (``venv.sh`` might not be compatible with other shells)
* GNU Make
* Podman and Buildah
* Helm (only to debug Helm templates)

First, create and enter the virtual environment::

    $ source venv.sh

Then you can use ``make`` with following targets.

Development targets:

* ``all`` - Reformat code with ``black`` & ``isort`` and launch ``tox`` (see below).
* ``tox`` - Launch all ``tox`` jobs (tests, coverage, quality)
* ``test`` - Launch tox tests and coverage only with the default system Python version (``python3 --version``)
* ``quality`` - Launch quality checks in tox.
* ``isort`` - Reformat import statements.
* ``black`` - Reformat the whole code to match black code style.

Django helpers:

* ``migrate`` - Run Django database migrations.
* ``create-admin`` - Create a Django super user (username = ``admin``, password = ``admin``).
* ``runserver`` - Run Django server for local development.

Distribution targets:

* ``clean`` - Remove all generated files and cache.
* ``dist`` - Create source (``*.tar.gz``) and binary (``*.whl``) distributions in the ``dist`` directory.

Virtualenv:

* ``venv`` - Create or update the virtual environment. Used by `` source venv.sh``.
* ``update-venv`` - Force update the virtual environment.

Requires Podman and Buildah:

* ``bump-deps`` - Bump versions of dependencies specified in the ``CONSTRAINTS`` variable.
* ``refresh-deps`` - Bump versions of all dependencies.
* ``build-images`` - Build container images (not used for local development yet).
* ``run-images`` - Run container images (not used for local development yet).
* ``stop-images`` - Stop container images.

Requires Helm:

* ``debug-helm`` - Debug helm templates.
