==========
Deployment
==========

Configuration
=============

* Production project configuration is in the `deploy_cfg directory <./deploy_cfg/>`_.
* Helm charts are located in the `k8s directory <./k8s/>`_.
* Dockerfiles for container images are in the `images directory <./images/>`_.

Environments
============

Both environments are Kubernetes instances.

* **Staging**:

  * Automatically deployed from the master branch.
  * Manually deployed from other branches.

* **Production**: Manually deployed from a protected tag.


Database
========

* Prepare a database user and password::

    $ kubectl exec -it postgres12-... -- psql -U postgres
    postgres=# CREATE USER survey WITH PASSWORD '<secure password>';
    postgres=# CREATE DATABASE survey WITH OWNER survey;

* Use the credentials in  ``databaseURL`` Helm value (see CI variables).

Image Pull Secret
=================

* `Create a deploy token <https://gitlab.com/tiliado/survey/-/settings/repository>`_ with the ``read_registry`` scope.
* Create a secret to pull images from GitLab registry::

    kubectl create secret docker-registry <name> \
    --docker-server=https://registry.gitlab.com \
    --docker-username=<token username> \
    --docker-password=<token value> \
    --docker-email=<some email>

* Use the secret name int ``pullSecrets`` Helm value (see CI variables).

CI Variables
============

* ``HELM_VALUES_FILE`` (type = file) - Helm values form K8s deployment.

  * ``domains`` - a list of domain names for the application
  * ``pullSecrets`` - a list of pull secrets to pull container images
  * ``djangoAdmins`` - a string containing comma separated list of ``name:e-mail`` pairs.
  * ``djangoSecret`` - a random Django secret string
  * ``databaseURL`` - a string describing a database connection (``psql://user:password@server:port/database``)
  * ``emailURL`` - a string describing a SMTP connection (``smtps://user:password@server:port``)

Django
======

Create a super user::

    $ kubectl -n survey-24207731 exec -it -c app pod/survey-... -- \
      django-admin createsuperuser --username <username> --email=<email>

Test e-mail sending::

    $ kubectl -n survey-24207731 exec -it -c app pod/survey-... -- \
      django-admin shell
    >>> from django.core.mail import send_mail
    send_mail('Email test', 'It works!', 'support@example.com', ['support@example.com'])
