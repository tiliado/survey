"""Django settings for production deployment."""
import socket

import environ

from survey.utils import markdownify_links_target_blank

ENV = environ.Env()

############
# Security #
############

SECRET_KEY = ENV.str("SECRET_KEY")
DEBUG = ENV.bool("DEBUG", default=False)
ALLOWED_HOSTS = ENV.list("ALLOWED_HOSTS", default=["*"])
CSRF_COOKIE_HTTPONLY = True
CSRF_COOKIE_SECURE = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_BROWSER_XSS_FILTER = True
SESSION_COOKIE_SECURE = True
# FIXME: XFF_STRICT = True
XFF_TRUSTED_PROXY_DEPTH = ENV.int("XFF_TRUSTED_PROXY_DEPTH", 2)
SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
DATA_UPLOAD_MAX_NUMBER_FIELDS = 50_000
SESSION_COOKIE_AGE = 31 * 24 * 3600

AUTH_PASSWORD_VALIDATORS = [
    {"NAME": "django.contrib.auth.password_validation.UserAttributeSimilarityValidator"},
    {"NAME": "django.contrib.auth.password_validation.MinimumLengthValidator"},
    {"NAME": "django.contrib.auth.password_validation.CommonPasswordValidator"},
    {"NAME": "django.contrib.auth.password_validation.NumericPasswordValidator"},
]

##########################
# Application definition #
##########################

ROOT_URLCONF = "deploy_cfg.urls"
WSGI_APPLICATION = "deploy_cfg.wsgi.application"

INSTALLED_APPS = [
    "polymorphic",
    "nested_admin",
    "django.contrib.admin",
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "django.contrib.sessions",
    "django.contrib.messages",
    "django.contrib.staticfiles",
    "multi_email_field",
    "markdownx",
    "markdownify.apps.MarkdownifyConfig",
    "survey",
    "bootstrap5",
]

MIDDLEWARE = [
    "django.middleware.security.SecurityMiddleware",
    "django.contrib.sessions.middleware.SessionMiddleware",
    "django.middleware.common.CommonMiddleware",
    "django.middleware.csrf.CsrfViewMiddleware",
    "django.contrib.auth.middleware.AuthenticationMiddleware",
    "django.contrib.messages.middleware.MessageMiddleware",
    "django.middleware.clickjacking.XFrameOptionsMiddleware",
]

TEMPLATES = [
    {
        "BACKEND": "django.template.backends.django.DjangoTemplates",
        "APP_DIRS": True,
        "OPTIONS": {
            "context_processors": [
                "django.template.context_processors.debug",
                "django.template.context_processors.request",
                "django.contrib.auth.context_processors.auth",
                "django.contrib.messages.context_processors.messages",
            ],
        },
    },
]

############
# Database #
############

DATABASES = {"default": ENV.db("DATABASE_URL")}

###################
# E-mail settings #
###################

ADMINS = [x.split(":") for x in ENV.list("DJANGO_ADMINS", default=[])]
SERVER_EMAIL = ENV.str("SERVER_EMAIL", "webmaster@localhost")
DEFAULT_FROM_EMAIL = ENV.str("DEFAULT_FROM_EMAIL", "webmaster@localhost")
EMAIL_SUBJECT_PREFIX = ENV.str("EMAIL_SUBJECT_PREFIX", f"[{socket.gethostname()}]: ")
vars().update(ENV.email_url("EMAIL_URL"))

########################
# Internationalization #
########################

LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_L10N = True
USE_TZ = True
USE_THOUSAND_SEPARATOR = True

################
# Static files #
################

STATICFILES_STORAGE = "django.contrib.staticfiles.storage.ManifestStaticFilesStorage"
STATIC_URL = "/static/"
STATIC_ROOT = "/app/static"

############
# Markdown #
############

MARKDOWNIFY = {
    "default": {
        "WHITELIST_TAGS": [
            "a",
            "abbr",
            "acronym",
            "b",
            "blockquote",
            "em",
            "i",
            "li",
            "ol",
            "p",
            "strong",
            "ul",
            "pre",
            "code",
        ],
        "WHITELIST_ATTRS": [
            "href",
            "src",
            "alt",
            "target",
        ],
        "MARKDOWN_EXTENSIONS": [
            "markdown.extensions.fenced_code",
            "markdown.extensions.extra",
        ],
        "LINKIFY_TEXT": {
            "PARSE_URLS": True,
            "CALLBACKS": [markdownify_links_target_blank],
        },
    }
}
