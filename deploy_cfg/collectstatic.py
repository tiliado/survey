"""Configuration for django-admin collectstatic."""
from os import environ

# Set dummy variables
environ.update(
    {
        "EMAIL_URL": "dummymail://",  # noqa: SC300
        "DATABASE_URL": "psql://user:pass@host/db",
        "CACHE_URL": "memcache://host:port",
        "SECRET_KEY": "...",
    }
)

# Import configuration
from .settings import *  # noqa
