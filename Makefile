# See README.rst - Local development
APP := survey
PKG := tiliado-survey
VENV_ROOT ?= ~/.virtualenvs
VENV_PATH ?= $(VENV_ROOT)/$(APP)
PYTHON_FILES = ${APP} setup.py deploy_cfg local_cfg
DEVEL_HOST ?= 127.0.0.1
DEVEL_PORT ?= 1080
IMAGE_TAG ?= latest
IMAGE_BUILD_ARGS ?= --pull --layers
UID := $(shell id -u)
GID := $(shell id -g)
# PIP constraints for bump/refresh targets.
CONSTRAINTS ?=
PIP_COMPILE_ARGS ?=

.PHONY: all tox test quality isort black clean dist venv venv-update migrate create-admin runserver build-images run-images

all: black isort tox

tox:
	tox --parallel all --parallel-live

test:
	tox -e clear-coverage,$(shell python3 -c 'from sys import version_info as v; print(f"py{v[0]}{v[1]}")'),compute-coverage

quality:
	tox -e quality

isort:
	isort ${PYTHON_FILES}

black:
	black ${PYTHON_FILES}

clean:
	rm -rf .tox .mypy_cache .coverage* htmlcov build dist out *.egg-info
	find . -name __pycache__ -type d -exec rm -rv {} +

dist: clean
	python3 setup.py sdist
	rm -rf build *.egg-info
	python3 setup.py bdist_wheel
	ls -lh dist

venv: $(VENV_PATH)/versions.txt

venv-update:
	rm -f $(VENV_PATH)/versions.txt
	make venv

$(VENV_PATH)/versions.txt: setup.py setup.cfg Makefile $(VENV_PATH)/bin/activate
	$(VENV_PATH)/bin/pip install -U pip setuptools wheel
	$(VENV_PATH)/bin/pip install -U -e .[devel]
	$(VENV_PATH)/bin/pip uninstall -y $(PKG)
	$(VENV_PATH)/bin/pip freeze > $(VENV_PATH)/versions.txt

$(VENV_PATH)/bin/activate:
	mkdir -p ${VENV_ROOT}
	test -d ${VENV_PATH} || python3 -m venv ${VENV_PATH}
	touch $(VENV_PATH)/bin/activate

migrate: venv
	$(VENV_PATH)/bin/python manage.py migrate

create-admin: migrate
	DJANGO_SUPERUSER_PASSWORD=admin $(VENV_PATH)/bin/python manage.py createsuperuser \
		--no-input --username=admin --email=admin@example.org

runserver: migrate
	$(VENV_PATH)/bin/python manage.py runserver "$(DEVEL_HOST):$(DEVEL_PORT)"

build-images:
	buildah bud ${IMAGE_BUILD_ARGS} -t "survey/app:${IMAGE_TAG}" -f images/app/Dockerfile .
	buildah bud ${IMAGE_BUILD_ARGS} -t "survey/nginx:${IMAGE_TAG}" -f images/nginx/Dockerfile \
		--build-arg "APP_VERSION=${IMAGE_TAG}" images/nginx

run-images: build-images stop-images
	mkdir -p build ~/.local/volumes/tiliado-survey-db ~/.local/volumes/tiliado-survey-mailhog
	sed \
		-e 's/runAsUser:/\0 ${UID}/g' \
		-e 's/runAsGroup:/\0 ${GID}/g' \
		-e 's/fsGroup:/\0 ${GID}/g' \
		-e 's#~#${HOME}#g' \
    	local_cfg/pod.yml | tee build/pod.yml
	podman play kube build/pod.yml

stop-images:
	podman pod ps --format='{{.ID}}' -f 'label=app=tiliado-survey' | xargs -r podman pod rm -f

debug-helm:
	helm template --debug --values k8s/debug.yaml \
	--set 'annotations.app\.gitlab\.com/app'=CI_PROJECT_PATH_SLUG \
	--set 'annotations.app\.gitlab\.com/env'=CI_ENVIRONMENT_SLUG \
	./k8s

refresh-deps:
	$(MAKE) PIP_COMPILE_ARGS="${PIP_COMPILE_ARGS} --upgrade" bump-deps

bump-deps: BUMP_IMAGE := survey/bump:${IMAGE_TAG}
bump-deps:
	buildah bud --pull --layers --target python-env -t "${BUMP_IMAGE}" -f images/app/Dockerfile .
	podman run \
		--rm \
		--user "${UID}:${GID}" \
		--volume "${PWD}/images/app:/app/build/data:Z" \
		--workdir /app/build/data \
		-e CUSTOM_COMPILE_COMMAND='`make bump-deps` or `make refresh-deps' \
		${BUMP_IMAGE} \
		pip-compile \
		--no-allow-unsafe \
		${PIP_COMPILE_ARGS} \
		$(foreach pkg,${CONSTRAINTS},-P '${pkg}') \
		requirements.in

	-git --no-pager diff "images/app/requirements.txt"
