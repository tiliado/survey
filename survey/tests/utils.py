from datetime import datetime, timezone, tzinfo
from typing import Optional

from django.template import engines


def parse_iso_datetime(iso_datetime: Optional[str], tz: tzinfo = timezone.utc) -> Optional[datetime]:
    return datetime.fromisoformat(iso_datetime).replace(tzinfo=tz) if iso_datetime else None


def template_from_string(template_string, using="django"):
    return engines[using].from_string(template_string)
