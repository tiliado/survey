from unittest.mock import call, patch

from django.core.exceptions import ImproperlyConfigured
from django.http import QueryDict
from django.test import SimpleTestCase
from django.utils.safestring import SafeString

from survey.forms import ShortAnswerWidget
from survey.forms.base import FormWidget, Renderable
from survey.models import Question, ShortAnswer
from survey.tests.utils import template_from_string


class RenderableTest(SimpleTestCase):
    def test_get_template_name_ok(self):
        renderer = Renderable()
        renderer.template_name = "template"
        self.assertEqual(renderer.get_template_name(), "template")

    def test_get_template_name_not_set(self):
        renderer = Renderable()
        self.assertRaisesMessage(
            ImproperlyConfigured, "No template provided for Renderable.", renderer.get_template_name
        )

    def test_context_data(self):
        renderer = Renderable()
        self.assertDictEqual(renderer.get_context_data(foo="bar"), {"foo": "bar"})

    @patch("survey.forms.base.get_template", return_value=template_from_string("<h1>{{ foo }}</h1>"))
    def test_render(self, get_template_mock):
        renderer = Renderable()
        renderer.template_name = "template.html"
        self.assertEqual(renderer.render(foo="bar"), SafeString("<h1>bar</h1>"))
        self.assertListEqual(get_template_mock.mock_calls, [call("template.html")])

    @patch("survey.forms.base.get_template", return_value=template_from_string("<h1>{{ foo }}</h1>"))
    def test_str(self, get_template_mock):
        renderer = Renderable()
        renderer.template_name = "template.html"
        self.assertEqual(str(renderer), SafeString("<h1></h1>"))
        self.assertListEqual(get_template_mock.mock_calls, [call("template.html")])

    def test_repr(self):
        renderer = Renderable()
        self.assertEqual(repr(renderer), "<Renderable>")


class FormWidgetTest(SimpleTestCase):
    def test_init_subclass(self):
        self.assertIs(FormWidget.factories[ShortAnswer], ShortAnswerWidget)

    def test_init_subclass_replace(self):
        class ShortAnswerWidget2(ShortAnswerWidget):
            pass

        self.assertIs(FormWidget.factories[ShortAnswer], ShortAnswerWidget2)

    def test_init_subclass_replace2(self):
        class ShortAnswerWidget3(FormWidget[ShortAnswer]):
            model_type = ShortAnswer

        self.assertIs(FormWidget.factories[ShortAnswer], ShortAnswerWidget3)

    def test_init_subclass_no_replace(self):
        FormWidget.factories[ShortAnswer] = ShortAnswerWidget

        class ShortAnswerWidget4(FormWidget[ShortAnswer]):
            pass

        self.assertIs(FormWidget.factories[ShortAnswer], ShortAnswerWidget)

    def test_create_for_model(self):
        self.assertIsInstance(FormWidget.create_for_model(ShortAnswer(), "q1", None, None), ShortAnswerWidget)

    def test_create_for_model_not_found(self):
        self.assertRaisesMessage(
            TypeError,
            "No widget found for <class 'survey.models.questions.Question'>.",
            FormWidget.create_for_model,
            Question(),
            "q1",
        )

    def test_get_template_name_default(self):
        widget = ShortAnswerWidget(ShortAnswer(), "q1", None, None)
        self.assertEqual(widget.get_template_name(), "survey/question/short_answer.html")

    def test_get_template_name_override(self):
        widget = ShortAnswerWidget(ShortAnswer(), "q1", None, None)
        widget.template_name = "template.html"
        self.assertEqual(widget.get_template_name(), "template.html")

    def test_get_context_data(self):
        widget = ShortAnswerWidget(ShortAnswer(), "q1", None, None)
        self.assertDictEqual(
            widget.get_context_data(foo="bar"),
            {
                "widget": widget,
                "question": widget.model,
                "data_key": "q1",
                "foo": "bar",
                "value": "",
            },
        )

    def test_repr(self):
        widget = ShortAnswerWidget(ShortAnswer(), "q1", None, None)
        self.assertEqual(repr(widget), "<ShortAnswerWidget: <ShortAnswer: None → None → ''>>")

    def test_serialize_data_none(self):
        widget = ShortAnswerWidget(ShortAnswer(), "q1", None, None)
        self.assertIs(widget.serialize_data(), None)

    def test_serialize_data_not_found(self):
        data = QueryDict("q0=foo")
        widget = ShortAnswerWidget(ShortAnswer(), "q1", data, None)
        self.assertListEqual(widget.serialize_data(), [])

    def test_serialize_data_one_value(self):
        data = QueryDict("q0=foo&q1=foo")
        widget = ShortAnswerWidget(ShortAnswer(), "q1", data, None)
        self.assertListEqual(widget.serialize_data(), ["foo"])

    def test_serialize_data_more_values(self):
        data = QueryDict("q1=foo&q0=foo&q1=bar")
        widget = ShortAnswerWidget(ShortAnswer(), "q1", data, None)
        self.assertListEqual(widget.serialize_data(), ["foo", "bar"])
