from unittest.mock import call, patch

from django.core.exceptions import ValidationError
from django.http.request import QueryDict
from django.test import SimpleTestCase, TestCase

from survey.forms import ChoiceGridWidget, LongAnswerWidget, MultipleChoiceWidget
from survey.forms.widgets import Choice, GridChoice, ShortAnswerWidget, ValidationMixin
from survey.models import ChoiceGrid, Entry, LongAnswer, MultipleChoice, Section, ShortAnswer, Survey


class ValidationMixinTest(SimpleTestCase):
    class MyValidationMixin(ValidationMixin[int]):
        def validate(self) -> int:
            return 1024  # pragma: nocover

    @patch.object(MyValidationMixin, "validate", side_effect=[1, 2])
    def test_valid_once(self, mock):
        m = ValidationMixinTest.MyValidationMixin()
        self.assertTrue(m.is_valid())
        self.assertIsNone(m.error)
        self.assertEqual(m.cleaned_data, 1)
        self.assertTrue(m.is_valid())
        self.assertIsNone(m.error)
        self.assertEqual(m.cleaned_data, 1)
        self.assertListEqual(mock.mock_calls, [call()])

    @patch.object(MyValidationMixin, "validate", side_effect=[ValidationError("Bananas!"), 2])
    def test_invalid_once(self, mock):
        m = ValidationMixinTest.MyValidationMixin()
        self.assertFalse(m.is_valid())
        self.assertListEqual(list(m.error), ["Bananas!"])
        self.assertIsNone(m.cleaned_data)
        self.assertFalse(m.is_valid())
        self.assertListEqual(list(m.error), ["Bananas!"])
        self.assertIsNone(m.cleaned_data)
        self.assertListEqual(mock.mock_calls, [call()])


class ShortAnswerWidgetTest(TestCase):
    def setUp(self) -> None:
        self.survey = Survey.objects.create(pk=1, slug="survey1", name="First Survey")
        self.section = Section.objects.create(pk=1, slug="section1", name="First Section", survey=self.survey)
        self.question = ShortAnswer.objects.create(pk=1, slug="q1", section=self.section)

    def test_value_default(self):
        widget = ShortAnswerWidget(self.question, "q1", None, None)
        self.assertEqual(widget.value, "")
        self.assertEqual(widget.get_context_data()["value"], "")

    def test_value_initial(self):
        widget = ShortAnswerWidget(self.question, "q1", None, ["cherry", "banana"])
        self.assertEqual(widget.value, "banana")
        self.assertEqual(widget.get_context_data()["value"], "banana")

    def test_value_post(self):
        widget = ShortAnswerWidget(self.question, "q1", QueryDict("q1=cherry&q1=banana"), None)
        self.assertEqual(widget.value, "banana")
        self.assertEqual(widget.get_context_data()["value"], "banana")

    def test_validate_no_data(self):
        self.question.required = False
        w = ShortAnswerWidget(self.question, "q1", None, None)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertIsNone(w.error)
        self.assertEqual(w.cleaned_data, "")
        self.assertEqual(w.get_cleaned_data(), "")

    def test_validate_empty_optional(self):
        self.question.required = False
        w = ShortAnswerWidget(self.question, "q1", QueryDict(), None)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertIsNone(w.error)
        self.assertEqual(w.cleaned_data, "")
        self.assertEqual(w.get_cleaned_data(), "")

    def test_validate_empty_required(self):
        self.question.required = True
        w = ShortAnswerWidget(self.question, "q1", QueryDict(), None)
        self.assertFalse(w.is_valid())
        self.assertTrue(w.has_error())
        self.assertListEqual(list(w.error), ["This answer is required."])
        self.assertIsNone(w.cleaned_data)
        self.assertIsNone(w.get_cleaned_data())
        self.assertIn("This answer is required.", str(w))

    def test_validate_not_empty_optional(self):
        self.question.required = False
        w = ShortAnswerWidget(self.question, "q1", QueryDict("q1=cherry&q1=banana"), None)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertIsNone(w.error)
        self.assertEqual(w.cleaned_data, "banana")
        self.assertEqual(w.get_cleaned_data(), "banana")

    def test_validate_not_empty_required(self):
        self.question.required = True
        w = ShortAnswerWidget(self.question, "q1", QueryDict("q1=cherry&q1=banana"), None)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertIsNone(w.error)
        self.assertEqual(w.cleaned_data, "banana")
        self.assertEqual(w.get_cleaned_data(), "banana")

    def test_validate_too_long(self):
        self.question.max_length = 5
        w = ShortAnswerWidget(self.question, "q1", QueryDict("q1=pineapple"), None)
        self.assertFalse(w.is_valid())
        self.assertTrue(w.has_error())
        self.assertListEqual(list(w.error), ["This answer is too long."])
        self.assertIsNone(w.cleaned_data)
        self.assertIsNone(w.get_cleaned_data())
        self.assertIn("This answer is too long.", str(w))


class LongAnswerWidgetTest(TestCase):
    def setUp(self) -> None:
        self.survey = Survey.objects.create(pk=1, slug="survey1", name="First Survey")
        self.section = Section.objects.create(pk=1, slug="section1", name="First Section", survey=self.survey)
        self.question = LongAnswer.objects.create(pk=1, slug="q1", section=self.section)

    def test_value_default(self):
        widget = LongAnswerWidget(self.question, "q1", None, None)
        self.assertEqual(widget.value, "")
        self.assertEqual(widget.get_context_data()["value"], "")

    def test_value_initial(self):
        widget = LongAnswerWidget(self.question, "q1", None, ["cherry", "banana"])
        self.assertEqual(widget.value, "banana")
        self.assertEqual(widget.get_context_data()["value"], "banana")

    def test_value_post(self):
        widget = LongAnswerWidget(self.question, "q1", QueryDict("q1=cherry&q1=banana"), None)
        self.assertEqual(widget.value, "banana")
        self.assertEqual(widget.get_context_data()["value"], "banana")

    def test_validate_empty_optional(self):
        self.question.required = False
        w = LongAnswerWidget(self.question, "q1", QueryDict(), None)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertIsNone(w.error)
        self.assertEqual(w.cleaned_data, "")
        self.assertEqual(w.get_cleaned_data(), "")

    def test_validate_empty_required(self):
        self.question.required = True
        w = LongAnswerWidget(self.question, "q1", QueryDict(), None)
        self.assertFalse(w.is_valid())
        self.assertTrue(w.has_error())
        self.assertListEqual(list(w.error), ["This answer is required."])
        self.assertIsNone(w.cleaned_data)
        self.assertIsNone(w.get_cleaned_data())
        self.assertIn("This answer is required.", str(w))

    def test_validate_not_empty_optional(self):
        self.question.required = False
        w = LongAnswerWidget(self.question, "q1", QueryDict("q1=cherry&q1=banana"), None)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertIsNone(w.error)
        self.assertEqual(w.cleaned_data, "banana")
        self.assertEqual(w.get_cleaned_data(), "banana")

    def test_validate_not_empty_required(self):
        self.question.required = True
        w = LongAnswerWidget(self.question, "q1", QueryDict("q1=cherry&q1=banana"), None)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertIsNone(w.error)
        self.assertEqual(w.cleaned_data, "banana")
        self.assertEqual(w.get_cleaned_data(), "banana")

    def test_validate_too_long(self):
        self.question.max_length = 5
        w = LongAnswerWidget(self.question, "q1", QueryDict("q1=pineapple"), None)
        self.assertFalse(w.is_valid())
        self.assertTrue(w.has_error())
        self.assertListEqual(list(w.error), ["This answer is too long."])
        self.assertIsNone(w.cleaned_data)
        self.assertIsNone(w.get_cleaned_data())
        self.assertIn("This answer is too long.", str(w))


class MultipleChoiceWidgetTest(TestCase):
    def setUp(self) -> None:
        self.survey = Survey.objects.create(pk=1, slug="1", name="First Survey")
        self.section = Section.objects.create(pk=1, slug="first", name="First Section", survey=self.survey)
        self.question = MultipleChoice.objects.create(pk=1, slug="first", section=self.section, selection_limit=2)
        self.options = [
            self.question.options.create(slug=f"opt{i}", label=f"Option {i}", selected=i == 1) for i in range(3)
        ]

    def test_initial_none(self):
        widget = MultipleChoiceWidget(self.question, "q1", None, None)
        self.assertListEqual(widget.initial, ["opt1"])

    def test_initial_dict(self):
        widget = MultipleChoiceWidget(self.question, "q1", None, {"apple": ["banana", "cherry"]})
        self.assertListEqual(widget.initial, ["opt1"])

    def test_initial_list(self):
        widget = MultipleChoiceWidget(self.question, "q1", None, ["banana", "cherry"])
        self.assertListEqual(widget.initial, ["banana", "cherry"])

    def test_context_data(self):
        widget = MultipleChoiceWidget(self.question, "q1", None, None)
        context = widget.get_context_data(foo="bar")
        self.assertEqual(context["question"], widget.model)
        self.assertEqual(context["data_key"], "q1")
        self.assertEqual(context["foo"], "bar")
        self.assertListEqual([x.slug for x in context["options"]], ["opt0", "opt1", "opt2"])

    def test_validate_unknown_option(self):
        w = MultipleChoiceWidget(self.question, "entry1", QueryDict("entry1=cherry&entry1=banana"), None)
        self.assertFalse(w.is_valid())
        self.assertTrue(w.has_error())
        self.assertIsNone(w.get_cleaned_data())
        self.assertIn("Invalid values: [&#x27;banana&#x27;, &#x27;cherry&#x27;].", str(w))

    def test_validate_too_many_selected(self):
        w = MultipleChoiceWidget(self.question, "entry1", QueryDict("entry1=opt1&entry1=opt2&entry1=opt0"), None)
        self.assertFalse(w.is_valid())
        self.assertTrue(w.has_error())
        self.assertIsNone(w.get_cleaned_data())
        self.assertIn("Too many options have been selected.", str(w))

    def test_validate_single_selected(self):
        w = MultipleChoiceWidget(self.question, "entry1", QueryDict("entry1=opt1"), None)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertListEqual(w.get_cleaned_data(), ["opt1"])

    def test_validate_multiple_selected_sorted(self):
        w = MultipleChoiceWidget(self.question, "entry1", QueryDict("entry1=opt1&entry1=opt0"), None)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertListEqual(w.get_cleaned_data(), ["opt0", "opt1"])

    def test_validate_none_selected_required(self):
        w = MultipleChoiceWidget(self.question, "entry1", QueryDict(), None)
        self.assertFalse(w.is_valid())
        self.assertTrue(w.has_error())
        self.assertIsNone(w.get_cleaned_data())
        self.assertIn("This answer is required.", str(w))

    def test_validate_none_selected_optional(self):
        self.question.required = False
        w = MultipleChoiceWidget(self.question, "entry1", QueryDict(), None)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertListEqual(w.get_cleaned_data(), [])


class ChoiceGridWidgetTest(TestCase):
    def setUp(self) -> None:
        self.survey = Survey.objects.create(pk=1, slug="survey1", name="First Survey")
        self.section = Section.objects.create(pk=1, slug="section1", name="First Section", survey=self.survey)
        self.question = ChoiceGrid.objects.create(pk=1, slug="question1", section=self.section, selection_limit=2)
        self.options = [
            self.question.options.create(slug=f"opt{i}", label=f"Option {i}", selected=i == 1) for i in range(3)
        ]
        self.entries = [self.question.entries.create(slug=f"entry{i}", label=f"Entry {i}") for i in range(3)]

    def test_init_initial_none(self):
        widget = ChoiceGridWidget(self.question, "question1", None, None)
        self.assertIs(widget.initial, None)
        self.assertListEqual(widget.choices["entry1"].initial, ["opt1"])

    def test_init_initial_dict(self):
        initial = {"entry1": ["banana"]}
        widget = ChoiceGridWidget(self.question, "question1", None, initial)
        self.assertIs(widget.initial, initial)
        self.assertIs(widget.choices["entry1"].initial, initial["entry1"])
        self.assertEqual(widget.choices["entry2"].initial, ["opt1"])

    def test_context_data(self):
        widget = ChoiceGridWidget(self.question, "q1", None, None)
        context = widget.get_context_data(foo="bar")
        self.assertEqual(context["question"], widget.model)
        self.assertEqual(context["data_key"], "q1")
        self.assertEqual(context["foo"], "bar")
        self.assertListEqual(list(context["choices"]), ["entry0", "entry1", "entry2"])

    def test_serialize_data_none(self):
        widget = ChoiceGridWidget(self.question, "question1", None, None)
        self.assertIs(widget.serialize_data(), None)

    def test_serialize_data_not_found(self):
        data = QueryDict("question1=banana")
        widget = ChoiceGridWidget(self.question, "question1", data, None)
        self.assertDictEqual(widget.serialize_data(), {"entry0": [], "entry1": [], "entry2": []})

    def test_serialize_data_one_value(self):
        data = QueryDict("question1.entry0=banana&question1.entry1=pear")
        widget = ChoiceGridWidget(self.question, "question1", data, None)
        self.assertDictEqual(widget.serialize_data(), {"entry0": ["banana"], "entry1": ["pear"], "entry2": []})

    def test_serialize_data_more_values(self):
        data = QueryDict("question1.entry0=banana&question1.entry1=pear&question1.entry1=cherry&question1.entry0=lemon")
        widget = ChoiceGridWidget(self.question, "question1", data, None)
        self.assertDictEqual(
            widget.serialize_data(), {"entry0": ["banana", "lemon"], "entry1": ["pear", "cherry"], "entry2": []}
        )

    def test_validate_all_valid(self):
        data = QueryDict("q1.entry0=opt1&q1.entry0=opt0&q1.entry1=opt2&q1.entry2=opt1")
        w = ChoiceGridWidget(self.question, "q1", data, None)
        self.assertTrue(w.is_valid())
        self.assertListEqual([x.error for x in w.choices.values()], [None, None, None])
        self.assertFalse(w.has_error())
        self.assertDictEqual(w.get_cleaned_data(), {"entry0": ["opt0", "opt1"], "entry1": ["opt2"], "entry2": ["opt1"]})

    def test_validate_all_invalid(self):
        data = QueryDict("q1.entry0=opt1&q1.entry0=opt0&q1.entry0=opt2&q1.entry2=cherry&q1.entry2=banana")
        w = ChoiceGridWidget(self.question, "q1", data, None)
        self.assertFalse(w.is_valid())
        self.assertListEqual(
            [list(x.error) for x in w.choices.values()],
            [
                ["Too many options have been selected."],
                ["This answer is required."],
                ["Invalid values: ['banana', 'cherry']."],
            ],
        )
        self.assertTrue(w.has_error())
        self.assertDictEqual(w.get_cleaned_data(), {"entry0": None, "entry1": None, "entry2": None})
        html = str(w)
        self.assertIn("Too many options have been selected.", html)
        self.assertIn("This answer is required.", html)
        self.assertIn("Invalid values: [&#x27;banana&#x27;, &#x27;cherry&#x27;].", html)

    def test_validate_mixed(self):
        data = QueryDict(
            "q1.entry0=opt1&q1.entry0=opt0&q1.entry0=opt2&q1.entry2=cherry&q1.entry2=banana&q1.entry1=opt2"
        )
        w = ChoiceGridWidget(self.question, "q1", data, None)
        self.assertFalse(w.is_valid())
        self.assertListEqual(
            [None if x.error is None else list(x.error) for x in w.choices.values()],
            [
                ["Too many options have been selected."],
                None,
                ["Invalid values: ['banana', 'cherry']."],
            ],
        )
        self.assertTrue(w.has_error())
        self.assertDictEqual(w.get_cleaned_data(), {"entry0": None, "entry1": ["opt2"], "entry2": None})
        html = str(w)
        self.assertIn("Too many options have been selected.", html)
        self.assertIn("Invalid values: [&#x27;banana&#x27;, &#x27;cherry&#x27;].", html)


class ChoiceTest(SimpleTestCase):
    def test_serialize_data_none(self):
        widget = Choice("entry1", None, None, set(), 0, True)
        self.assertIs(widget.serialize_data(), None)

    def test_serialize_data_not_found(self):
        data = QueryDict("entry0=banana")
        widget = Choice("entry1", data, None, set(), 0, True)
        self.assertListEqual(widget.serialize_data(), [])

    def test_serialize_data_one_value(self):
        data = QueryDict("entry0=banana&entry1=banana")
        widget = Choice("entry1", data, None, set(), 0, True)
        self.assertListEqual(widget.serialize_data(), ["banana"])

    def test_serialize_data_more_values(self):
        data = QueryDict("entry1=banana&entry0=banana&entry1=pear")
        widget = Choice("entry1", data, None, set(), 0, True)
        self.assertListEqual(widget.serialize_data(), ["banana", "pear"])

    def test_values_default(self):
        widget = Choice("entry1", None, None, set(), 0, True)
        self.assertEqual(widget.values, [])

    def test_values_initial(self):
        widget = Choice("entry1", None, ["cherry", "banana"], set(), 0, True)
        self.assertEqual(widget.values, ["cherry", "banana"])

    def test_values_post(self):
        widget = Choice("entry1", QueryDict("entry1=cherry&entry1=banana"), None, set(), 0, True)
        self.assertEqual(widget.values, ["cherry", "banana"])

    def test_validate_unknown_option(self):
        w = Choice("entry1", QueryDict("entry1=cherry&entry1=banana"), None, {"apple"}, 1, True)
        self.assertFalse(w.is_valid())
        self.assertIsNone(w.cleaned_data)
        self.assertListEqual(list(w.error), ["Invalid values: ['banana', 'cherry']."])

    def test_validate_too_many_selected(self):
        w = Choice("entry1", QueryDict("entry1=cherry&entry1=pear"), None, {"cherry", "banana", "pear"}, 1, True)
        self.assertFalse(w.is_valid())
        self.assertIsNone(w.cleaned_data)
        self.assertListEqual(list(w.error), ["Too many options have been selected."])

    def test_validate_single_selected(self):
        w = Choice("entry1", QueryDict("entry1=cherry"), None, {"cherry", "banana", "pear"}, 2, True)
        self.assertTrue(w.is_valid())
        self.assertEqual(w.cleaned_data, ["cherry"])
        self.assertIsNone(w.error)

    def test_validate_multiple_selected_sorted(self):
        w = Choice("entry1", QueryDict("entry1=pear&entry1=cherry"), None, {"cherry", "banana", "pear"}, 2, True)
        self.assertTrue(w.is_valid())
        self.assertEqual(w.cleaned_data, ["cherry", "pear"])
        self.assertIsNone(w.error)

    def test_validate_none_selected_required(self):
        w = Choice("entry1", QueryDict(), None, {"apple"}, 2, True)
        self.assertFalse(w.is_valid())
        self.assertIsNone(w.cleaned_data)
        self.assertListEqual(list(w.error), ["This answer is required."])

    def test_validate_none_selected_optional(self):
        w = Choice("entry1", QueryDict(), None, {"cherry", "banana", "pear"}, 2, False)
        self.assertTrue(w.is_valid())
        self.assertEqual(w.cleaned_data, [])
        self.assertIsNone(w.error)


class GridChoiceTest(SimpleTestCase):
    def test_init(self) -> None:
        entry = Entry()
        self.assertIs(GridChoice(entry, "entry1", None, None, set(), 0, True).model, entry)
