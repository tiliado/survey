from django.http import QueryDict
from django.test import TestCase

from survey.forms import FormSection, SurveyForm
from survey.models import Section, ShortAnswer, Survey


class SurveyFormTest(TestCase):
    def setUp(self) -> None:
        self.survey = Survey.objects.create(pk=1, slug="survey1", name="First Survey")
        self.section = Section.objects.create(pk=1, slug="section1", name="First Section", survey=self.survey)
        self.section2 = Section.objects.create(pk=2, slug="section2", name="Second Section", survey=self.survey)
        self.questions = [
            ShortAnswer.objects.create(slug=f"q{i}", label=f"Question {i}", section=self.section) for i in range(3)
        ]

    def test_init_initial_none(self):
        form = SurveyForm(self.survey, initial=None)
        self.assertDictEqual(form.initial, {})
        self.assertDictEqual(form.sections["section1"].initial, {})

    def test_init_initial_dict(self):
        initial = {"section1": {"q1": ["banana"]}}
        form = SurveyForm(self.survey, initial=initial)
        self.assertIs(form.initial, initial)
        self.assertDictEqual(form.sections["section1"].initial, initial["section1"])

    def test_context_data(self):
        form = SurveyForm(self.survey)
        self.assertDictEqual(
            form.get_context_data(foo="bar"),
            {
                "survey": self.survey,
                "sections": form.sections,
                "data_key": "survey1",
                "foo": "bar",
                "has_error": False,
                "expiration_date": None,
            },
        )

    def test_context_data_with_prefix(self):
        form = SurveyForm(self.survey, prefix="survey")
        self.assertDictEqual(
            form.get_context_data(foo="bar"),
            {
                "survey": self.survey,
                "sections": form.sections,
                "data_key": "survey.survey1",
                "foo": "bar",
                "has_error": False,
                "expiration_date": None,
            },
        )

    def test_repr(self):
        form = SurveyForm(self.survey)
        self.assertEqual(repr(form), "<SurveyForm: <Survey: 'First Survey'>>")

    def test_serialize_data_none(self):
        widget = SurveyForm(self.survey, prefix="survey", data=None)
        self.assertIs(widget.serialize_data(), None)

    def test_serialize_data_not_found(self):
        data = QueryDict("survey1.section1.q1=foo")
        widget = SurveyForm(self.survey, prefix="survey", data=data)
        self.assertDictEqual(widget.serialize_data(), {"section1": {"q0": [], "q1": [], "q2": []}, "section2": {}})

    def test_serialize_data_one_value(self):
        data = QueryDict("survey.survey1.section1.q0=foo&survey.survey1.section1.q1=pear")
        widget = SurveyForm(self.survey, prefix="survey", data=data)
        self.assertDictEqual(
            widget.serialize_data(), {"section1": {"q0": ["foo"], "q1": ["pear"], "q2": []}, "section2": {}}
        )

    def test_serialize_data_more_values(self):
        data = QueryDict(
            "survey.survey1.section1.q0=foo&survey.survey1.section1.q1=pear"
            "&survey.survey1.section1.q1=cherry&survey.survey1.section1.q0=lemon"
        )
        widget = SurveyForm(self.survey, prefix="survey", data=data)
        self.assertDictEqual(
            widget.serialize_data(),
            {"section1": {"q0": ["foo", "lemon"], "q1": ["pear", "cherry"], "q2": []}, "section2": {}},
        )

    def test_validate_all_valid(self):
        data = QueryDict(
            "survey.survey1.section1.q0=cherry&" "survey.survey1.section1.q2=apple&" "survey.survey1.section1.q1=banana"
        )
        w = SurveyForm(self.survey, prefix="survey", data=data)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertFalse(w.get_context_data()["has_error"])
        self.assertDictEqual(
            w.get_cleaned_data(), {"section1": {"q0": "cherry", "q1": "banana", "q2": "apple"}, "section2": {}}
        )
        self.assertNotIn("There are a few errors", str(w))

    def test_validate_all_invalid(self):
        data = QueryDict("survey.survey1.section1.q0=")
        w = SurveyForm(self.survey, prefix="survey", data=data)
        self.assertFalse(w.is_valid())
        self.assertTrue(w.has_error())
        self.assertTrue(w.get_context_data()["has_error"])
        self.assertDictEqual(w.get_cleaned_data(), {"section1": {"q0": None, "q1": None, "q2": None}, "section2": {}})
        html = str(w)
        self.assertEqual(html.count("This answer is required."), 3)
        self.assertEqual(html.count("There are a few errors in your responses."), 1)
        self.assertEqual(html.count("There are a few errors in this section."), 1)

    def test_validate_mixed(self):
        data = QueryDict("survey.survey1.section1.q0=cherry&" "survey.survey1.section1.q2=apple")
        w = SurveyForm(self.survey, prefix="survey", data=data)
        self.assertFalse(w.is_valid())
        self.assertTrue(w.has_error())
        self.assertTrue(w.get_context_data()["has_error"])
        self.assertDictEqual(
            w.get_cleaned_data(), {"section1": {"q0": "cherry", "q1": None, "q2": "apple"}, "section2": {}}
        )
        html = str(w)
        self.assertEqual(html.count("This answer is required."), 1)
        self.assertEqual(html.count("There are a few errors in your responses."), 1)
        self.assertEqual(html.count("There are a few errors in this section."), 1)


class FormSectionTest(TestCase):
    def setUp(self) -> None:
        self.survey = Survey.objects.create(pk=1, slug="1", name="First Survey")
        self.section = Section.objects.create(pk=1, slug="first", name="First Section", survey=self.survey)
        self.questions = [
            ShortAnswer.objects.create(slug=f"q{i}", label=f"Question {i}", section=self.section, required=True)
            for i in range(3)
        ]

    def test_init_initial_none(self):
        form = FormSection(self.section, "section1", None, None)
        self.assertDictEqual(form.initial, {})
        self.assertIs(form.questions["q1"].initial, None)

    def test_init_initial_dict(self):
        initial = {"q1": ["banana"]}
        form = FormSection(self.section, "section1", None, initial)
        self.assertIs(form.initial, initial)
        self.assertIs(form.questions["q1"].initial, initial["q1"])

    def test_context_data(self):
        s = FormSection(self.section, "section1", None, None)
        self.assertDictEqual(
            s.get_context_data(foo="bar"),
            {
                "section": self.section,
                "questions": s.questions,
                "data_key": "section1",
                "foo": "bar",
                "has_error": False,
            },
        )

    def test_repr(self):
        form = FormSection(self.section, "section1", None, None)
        self.assertEqual(repr(form), "<FormSection: <Section: 'First Survey' → 'First Section'>>")

    def test_serialize_data_none(self):
        widget = FormSection(self.section, "section1", None, None)
        self.assertIs(widget.serialize_data(), None)

    def test_serialize_data_not_found(self):
        data = QueryDict("section1.q10=banana")
        widget = FormSection(self.section, "section1", data, None)
        self.assertDictEqual(widget.serialize_data(), {"q0": [], "q1": [], "q2": []})

    def test_serialize_data_one_value(self):
        data = QueryDict("section1.q0=banana&section1.q1=pear")
        widget = FormSection(self.section, "section1", data, None)
        self.assertDictEqual(widget.serialize_data(), {"q0": ["banana"], "q1": ["pear"], "q2": []})

    def test_serialize_data_more_values(self):
        data = QueryDict("section1.q0=banana&section1.q1=pear&section1.q1=cherry&section1.q0=lemon")
        widget = FormSection(self.section, "section1", data, None)
        self.assertDictEqual(widget.serialize_data(), {"q0": ["banana", "lemon"], "q1": ["pear", "cherry"], "q2": []})

    def test_validate_all_valid(self):
        data = QueryDict("section1.q0=cherry&section1.q2=apple&section1.q1=banana")
        w = FormSection(self.section, "section1", data, None)
        self.assertTrue(w.is_valid())
        self.assertFalse(w.has_error())
        self.assertFalse(w.get_context_data()["has_error"])
        self.assertDictEqual(w.get_cleaned_data(), {"q0": "cherry", "q1": "banana", "q2": "apple"})
        self.assertNotIn("There are a few errors", str(w))

    def test_validate_all_invalid(self):
        data = QueryDict("section1.q0=")
        w = FormSection(self.section, "section1", data, None)
        self.assertFalse(w.is_valid())
        self.assertTrue(w.has_error())
        self.assertTrue(w.get_context_data()["has_error"])
        self.assertDictEqual(w.get_cleaned_data(), {"q0": None, "q1": None, "q2": None})
        html = str(w)
        self.assertEqual(html.count("This answer is required."), 3)
        self.assertEqual(html.count("There are a few errors in this section."), 1)

    def test_validate_mixed(self):
        data = QueryDict("section1.q0=cherry&section1.q2=apple")
        w = FormSection(self.section, "section1", data, None)
        self.assertFalse(w.is_valid())
        self.assertTrue(w.has_error())
        self.assertTrue(w.get_context_data()["has_error"])
        self.assertDictEqual(w.get_cleaned_data(), {"q0": "cherry", "q1": None, "q2": "apple"})
        html = str(w)
        self.assertEqual(html.count("This answer is required."), 1)
        self.assertEqual(html.count("There are a few errors in this section."), 1)
