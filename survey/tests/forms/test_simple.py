from django.core.exceptions import ValidationError
from django.test import SimpleTestCase

from survey.forms.simple import validate_survey_key


class ValidateTest(SimpleTestCase):
    def test_validate_survey_key_valid(self):
        self.assertIsNone(validate_survey_key("my-key"))

    def test_validate_survey_key_invalid(self):
        self.assertRaises(ValidationError, validate_survey_key, "...")
