from unittest.mock import Mock, PropertyMock, call, patch, sentinel

from django.contrib import admin
from django.contrib.auth import get_user_model
from django.test import RequestFactory, SimpleTestCase, TestCase

from survey.admin import ResponseAdmin, SurveyAdmin
from survey.admin.survey import SurveyOpenFilter
from survey.models import Response, Survey


class SurveyOpenFilterTest(TestCase):
    request_factory = RequestFactory()

    def setUp(self):
        self.model = Survey
        self.model_admin = admin.ModelAdmin(self.model, admin.site)

    def test_lookups(self):
        request = self.request_factory.get("/")
        f = SurveyOpenFilter(request, {}, self.model, self.model_admin)
        self.assertEqual(
            f.lookups(request, self.model_admin),
            [("true", "Open"), ("false", "Closed")],
        )

    def test_queryset_default(self):
        request = self.request_factory.get("/")
        f = SurveyOpenFilter(request, {}, self.model, self.model_admin)
        qs = self.model.objects.all()
        self.assertIs(f.queryset(request, qs), qs)

    @patch.object(Survey, "filter_open", return_value=sentinel.filter_open)
    def test_queryset_open(self, mock):
        params = {"is_open": "true"}
        request = self.request_factory.get("/", params)
        f = SurveyOpenFilter(request, params, self.model, self.model_admin)
        qs = self.model.objects.all()
        self.assertIs(f.queryset(request, qs), sentinel.filter_open)
        self.assertListEqual(mock.mock_calls, [call(qs)])

    @patch.object(Survey, "filter_closed", return_value=sentinel.filter_closed)
    def test_queryset_closed(self, mock):
        params = {"is_open": "false"}
        request = self.request_factory.get("/", params)
        f = SurveyOpenFilter(request, params, self.model, self.model_admin)
        qs = self.model.objects.all()
        self.assertIs(f.queryset(request, qs), sentinel.filter_closed)
        self.assertListEqual(mock.mock_calls, [call(qs)])


class SurveyAdminTest(SimpleTestCase):
    def setUp(self):
        self.model = Survey
        self.model_admin = SurveyAdmin(self.model, admin.site)

    def test_is_open(self):
        obj = Mock()
        type(obj).is_open = is_open_mock = PropertyMock(return_value=sentinel.result)
        self.assertIs(self.model_admin.is_open(obj), sentinel.result)
        self.assertListEqual(is_open_mock.mock_calls, [call()])


class ResponseAdminTest(SimpleTestCase):
    def setUp(self):
        self.model = Response
        self.model_admin = ResponseAdmin(self.model, admin.site)

    def test_has_add_permission(self):
        User = get_user_model()
        request = RequestFactory().post("/")
        request.user = User(is_staff=True, is_active=True, is_superuser=True)
        self.assertFalse(self.model_admin.has_add_permission(request))

    def test_has_change_permission(self):
        User = get_user_model()
        request = RequestFactory().post("/")
        request.user = User(is_staff=True, is_active=True, is_superuser=True)
        self.assertFalse(self.model_admin.has_change_permission(request))

    def test_code_verbatim(self):
        r = self.model(code="<p>beef</p>")
        self.assertEqual(self.model_admin.code_verbatim(r), "<code>&lt;p&gt;beef&lt;/p&gt;</code>")

    def test_response_verbatim(self):
        r = self.model(response="<p>beef</p>")
        self.assertEqual(self.model_admin.response_verbatim(r), "<pre>&lt;p&gt;beef&lt;/p&gt;</pre>")
