from smtplib import SMTPException
from unittest.mock import call, patch

from django.contrib.messages import get_messages
from django.core import mail
from django.http import Http404
from django.http.request import QueryDict
from django.test import Client, RequestFactory, TestCase
from django.urls import reverse
from freezegun import freeze_time

from survey.forms import SurveyEnterForm, SurveyForm
from survey.models import Response, ShortAnswer, Survey
from survey.tests.const import SURVEY_CODE, SURVEY_KEY_MIXED
from survey.tests.utils import parse_iso_datetime
from survey.views import SurveyDetailView, SurveyListView


class SurveyListViewTest(TestCase):
    @patch.object(Survey, "filter_open")
    def test_get_queryset(self, mock):
        """Only public open surveys."""
        Survey.objects.create(pk=1, slug="1", public=True)
        Survey.objects.create(pk=2, slug="2", public=False)
        mock.return_value = Survey.objects.order_by("pk").all()
        result = SurveyListView().get_queryset()
        self.assertQuerysetEqual(result.values_list("pk", flat=True), (1,), transform=int)
        mock.assert_called_once()


@freeze_time("2000-01-14 12:00:01")
class SurveyDetailViewTest(TestCase):
    request_factory = RequestFactory()

    def setUp(self) -> None:
        self.client = Client()
        self.surveys = [
            Survey.objects.create(
                slug="survey0",
                public=True,
                name="Public Survey",
                start_date=parse_iso_datetime("1999-01-14 12:00:01"),
                report_email=["cherry@example.com", "banana@example.com"],
            ),
            Survey.objects.create(
                slug="survey1",
                public=False,
                name="Private Survey",
                start_date=parse_iso_datetime("1999-01-14 12:00:01"),
            ),
            Survey.objects.create(slug="survey2", public=True, name="Unpublished Survey"),
        ]

    def test_get_session_key_prefix_default(self):
        view = SurveyDetailView()
        view.survey = self.surveys[0]
        self.assertEqual(view.get_session_key(), "survey.survey0")

    def test_get_session_key_prefix_none(self):
        view = SurveyDetailView()
        view.survey = self.surveys[0]
        view.session_prefix = None
        self.assertEqual(view.get_session_key(), "survey0")

    def test_get_session_key_prefix_overridden(self):
        view = SurveyDetailView()
        view.survey = self.surveys[0]
        view.session_prefix = "cherry"
        self.assertEqual(view.get_session_key(), "cherry.survey0")

    def test_get_survey_public(self):
        view = SurveyDetailView()
        view.kwargs = {"slug": "survey0"}
        self.assertEqual(view.get_survey(), self.surveys[0])

    def test_get_survey_private(self):
        view = SurveyDetailView()
        view.kwargs = {"slug": "survey1"}
        self.assertEqual(view.get_survey(), self.surveys[1])

    def test_get_survey_not_started(self):
        view = SurveyDetailView()
        view.kwargs = {"slug": "survey3"}
        self.assertRaises(Http404, view.get_survey)

    def test_get_survey_not_found(self):
        view = SurveyDetailView()
        view.kwargs = {"slug": "surveyX"}
        self.assertRaises(Http404, view.get_survey)

    def test_get_form(self):
        view = SurveyDetailView()
        view.request = self.request_factory.get("/")
        view.request.session = self.client.session
        view.survey = self.surveys[0]
        form = view.get_form()
        self.assertIsInstance(form, SurveyForm)
        self.assertEqual(form.data_key, "survey0")
        self.assertIs(form.data, None)

    def test_get_form_with_data(self):
        view = SurveyDetailView()
        view.request = self.request_factory.get("/")
        view.request.session = self.client.session
        view.survey = self.surveys[0]
        data = QueryDict()
        form = view.get_form(data=data)
        self.assertIsInstance(form, SurveyForm)
        self.assertEqual(form.data_key, "survey0")
        self.assertIs(form.data, data)

    def test_get_form_with_initial(self):
        view = SurveyDetailView()
        view.request = self.request_factory.get("/")
        view.request.session = self.client.session
        view.survey = self.surveys[0]
        initial = {}
        form = view.get_form(initial=initial)
        self.assertIsInstance(form, SurveyForm)
        self.assertEqual(form.data_key, "survey0")
        self.assertIs(form.initial, initial)

    def test_get_not_found(self):
        response = self.client.get(reverse("survey:survey-detail", args=("surveyX", SURVEY_CODE)))
        self.assertEqual(response.status_code, 404)
        self.assertNotIn("survey", response.context)
        self.assertNotIn("form", response.context)
        self.assertDictEqual(self.client.session.load(), {})

    @patch("survey.utils.normalize_survey_code", side_effect=[ValueError("Invalid code")])
    @patch("survey.views.LOGGER.warning")
    def test_get_invalid_code(self, log_mock, norm_mock):
        response = self.client.get(reverse("survey:survey-detail", args=("surveyX", "blah")))
        redirect_url = reverse("survey:survey-enter", args=("surveyX",))
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
        self.assertDictEqual(self.client.session.load(), {})
        self.assertListEqual(norm_mock.mock_calls, [call("blah")])
        self.assertListEqual(log_mock.mock_calls, [call("Invalid survey code: %r.", "blah", exc_info=True)])

    def test_get(self):
        response = self.client.get(reverse("survey:survey-detail", args=("survey0", SURVEY_CODE)))
        self.assertContains(response, "<title>Survey: Public Survey</title>")
        self.assertEqual(response.context["survey"], self.surveys[0])
        self.assertIsInstance(response.context["form"], SurveyForm)
        self.assertDictEqual(response.context["form"].initial, {})
        self.assertIsNotNone(response.context["form"].expiration_date)
        self.assertDictEqual(self.client.session.load(), {})

    def test_get_with_session(self):
        session = self.client.session
        session["survey.survey0"] = {"cherry": "banana"}
        session.save()
        response = self.client.get(reverse("survey:survey-detail", args=("survey0", SURVEY_CODE)))

        self.assertContains(response, "<title>Survey: Public Survey</title>")
        self.assertEqual(response.context["survey"], self.surveys[0])
        self.assertIsInstance(response.context["form"], SurveyForm)
        self.assertDictEqual(response.context["form"].initial, {"cherry": "banana"})
        self.assertIsNotNone(response.context["form"].expiration_date)
        self.assertDictEqual(session.load(), {"survey.survey0": {"cherry": "banana"}})

    def test_post_not_found(self):
        response = self.client.post(reverse("survey:survey-detail", args=("surveyX", SURVEY_CODE)))
        self.assertEqual(response.status_code, 404)
        self.assertNotIn("survey", response.context)
        self.assertNotIn("form", response.context)
        self.assertDictEqual(self.client.session.load(), {})
        self.assertFalse(Response.objects.exists())

    def test_post_save(self):
        section = self.surveys[0].sections.create(slug="section0")
        ShortAnswer.objects.create(section=section, slug="q0", required=True)
        session = self.client.session
        session["survey.survey0"] = {"cherry": "banana"}
        session.save()
        response = self.client.post(
            reverse("survey:survey-detail", args=("survey0", SURVEY_CODE)),
            data={"survey0.section0.q0": "", "save": "yes"},
        )

        self.assertContains(response, "<title>Survey: Public Survey</title>")
        self.assertEqual(response.context["survey"], self.surveys[0])
        self.assertIsInstance(response.context["form"], SurveyForm)
        self.assertDictEqual(response.context["form"].initial, {})
        self.assertDictEqual(session.load(), {"survey.survey0": {"section0": {"q0": [""]}}})
        self.assertEqual(len(mail.outbox), 0)
        self.assertFalse(response.context["form"].has_error())
        self.assertIsNotNone(response.context["form"].expiration_date)
        self.assertFalse(Response.objects.exists())

    def test_post_invalid(self):
        section = self.surveys[0].sections.create(slug="section0")
        ShortAnswer.objects.create(section=section, slug="q0", required=True)
        session = self.client.session
        session["survey.survey0"] = {"cherry": "banana"}
        session.save()
        response = self.client.post(
            reverse("survey:survey-detail", args=("survey0", SURVEY_CODE)), data={"survey0.section0.q0": ""}
        )

        self.assertContains(response, "<title>Survey: Public Survey</title>")
        self.assertEqual(response.context["survey"], self.surveys[0])
        self.assertIsInstance(response.context["form"], SurveyForm)
        self.assertDictEqual(response.context["form"].initial, {})
        self.assertDictEqual(session.load(), {"survey.survey0": {"section0": {"q0": [""]}}})
        self.assertEqual(len(mail.outbox), 0)
        self.assertIsNotNone(response.context["form"].expiration_date)
        self.assertFalse(response.context["form"].is_valid())
        self.assertFalse(Response.objects.exists())

    def test_post_valid(self):
        section = self.surveys[0].sections.create(slug="section0")
        ShortAnswer.objects.create(section=section, slug="q0", required=True)
        session = self.client.session
        session["survey.survey0"] = {"cherry": "banana"}
        session.save()
        response = self.client.post(
            reverse("survey:survey-detail", args=("survey0", SURVEY_CODE)), data={"survey0.section0.q0": "foo"}
        )

        self.assertRedirects(
            response, reverse("survey:survey-submitted", args=("survey0", SURVEY_CODE)), fetch_redirect_response=False
        )
        self.assertDictEqual(session.load(), {})
        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(mail.outbox[0].subject, "New survey result for Public Survey")
        self.assertEqual(mail.outbox[1].subject, "New survey result for Public Survey")
        self.assertEqual(mail.outbox[0].to, ["cherry@example.com"])
        self.assertEqual(mail.outbox[1].to, ["banana@example.com"])
        body = '{\n  "code": "' + SURVEY_CODE + '",\n  "data": {\n    "section0": {\n      "q0": "foo"\n    }\n  }\n}'
        self.assertEqual(mail.outbox[0].body, body)
        self.assertEqual(mail.outbox[1].body, body)

        self.assertQuerysetEqual(
            Response.objects.all().values_list("survey__slug", "code", "response"),
            [("survey0", SURVEY_CODE, '{\n  "section0": {\n    "q0": "foo"\n  }\n}')],
            transform=tuple,
            ordered=False,
        )

    @patch("survey.views.send_mail", side_effect=[ConnectionError("Connection refused")])
    @patch("survey.views.LOGGER.exception")
    def test_post_valid_connection_error(self, log_mock, mail_mock):
        section = self.surveys[0].sections.create(slug="section0")
        ShortAnswer.objects.create(section=section, slug="q0", required=True)
        session = self.client.session

        response = self.client.post(
            reverse("survey:survey-detail", args=("survey0", SURVEY_CODE)), data={"survey0.section0.q0": "foo"}
        )
        self.assertRedirects(
            response, reverse("survey:survey-submitted", args=("survey0", SURVEY_CODE)), fetch_redirect_response=False
        )
        self.assertDictEqual(session.load(), {})
        body = '{\n  "code": "' + SURVEY_CODE + '",\n  "data": {\n    "section0": {\n      "q0": "foo"\n    }\n  }\n}'
        self.assertListEqual(
            mail_mock.mock_calls,
            [
                call("New survey result for Public Survey", body, None, ["cherry@example.com"]),
                call("New survey result for Public Survey", body, None, ["banana@example.com"]),
            ],
        )
        self.assertListEqual(
            log_mock.mock_calls,
            [
                call("Failed to send survey %r results to %r.", "Public Survey", "cherry@example.com"),
                call("Failed to send survey %r results to %r.", "Public Survey", "banana@example.com"),
            ],
        )
        self.assertQuerysetEqual(
            Response.objects.all().values_list("survey__slug", "code", "response"),
            [("survey0", SURVEY_CODE, '{\n  "section0": {\n    "q0": "foo"\n  }\n}')],
            transform=tuple,
            ordered=False,
        )

    @patch("survey.views.send_mail", side_effect=[SMTPException("SMTP Error")])
    @patch("survey.views.LOGGER.exception")
    def test_post_valid_smtp_error(self, log_mock, mail_mock):
        section = self.surveys[0].sections.create(slug="section0")
        ShortAnswer.objects.create(section=section, slug="q0", required=True)
        session = self.client.session

        response = self.client.post(
            reverse("survey:survey-detail", args=("survey0", SURVEY_CODE)), data={"survey0.section0.q0": "foo"}
        )
        self.assertRedirects(
            response, reverse("survey:survey-submitted", args=("survey0", SURVEY_CODE)), fetch_redirect_response=False
        )
        self.assertDictEqual(session.load(), {})
        body = '{\n  "code": "' + SURVEY_CODE + '",\n  "data": {\n    "section0": {\n      "q0": "foo"\n    }\n  }\n}'
        self.assertListEqual(
            mail_mock.mock_calls,
            [
                call("New survey result for Public Survey", body, None, ["cherry@example.com"]),
                call("New survey result for Public Survey", body, None, ["banana@example.com"]),
            ],
        )
        self.assertListEqual(
            log_mock.mock_calls,
            [
                call("Failed to send survey %r results to %r.", "Public Survey", "cherry@example.com"),
                call("Failed to send survey %r results to %r.", "Public Survey", "banana@example.com"),
            ],
        )
        self.assertQuerysetEqual(
            Response.objects.all().values_list("survey__slug", "code", "response"),
            [("survey0", SURVEY_CODE, '{\n  "section0": {\n    "q0": "foo"\n  }\n}')],
            transform=tuple,
            ordered=False,
        )

    @patch("survey.views.send_mail")
    def test_post_valid_no_report_mail(self, mock):
        self.surveys[0].report_email = None
        self.surveys[0].save()

        section = self.surveys[0].sections.create(slug="section0")
        ShortAnswer.objects.create(section=section, slug="q0", required=True)
        session = self.client.session

        response = self.client.post(
            reverse("survey:survey-detail", args=("survey0", SURVEY_CODE)), data={"survey0.section0.q0": "foo"}
        )
        self.assertRedirects(
            response, reverse("survey:survey-submitted", args=("survey0", SURVEY_CODE)), fetch_redirect_response=False
        )
        self.assertDictEqual(session.load(), {})
        self.assertListEqual(mock.mock_calls, [])

        self.assertQuerysetEqual(
            Response.objects.all().values_list("survey__slug", "code", "response"),
            [("survey0", SURVEY_CODE, '{\n  "section0": {\n    "q0": "foo"\n  }\n}')],
            transform=tuple,
            ordered=False,
        )

    def test_get_response_exists(self):
        self.surveys[0].responses.create(response="{}", code=SURVEY_CODE)
        response = self.client.get(reverse("survey:survey-detail", args=("survey0", SURVEY_CODE)))
        self.assertRedirects(response, reverse("survey:survey-list"), fetch_redirect_response=False)
        messages = [str(x) for x in get_messages(response.wsgi_request)]
        self.assertListEqual(messages, ["You have already responded to survey Public Survey."])
        self.assertQuerysetEqual(
            Response.objects.all().values_list("survey__slug", "code", "response"),
            [("survey0", SURVEY_CODE, "{}")],
            transform=tuple,
            ordered=False,
        )

    def test_post_response_exists(self):
        self.surveys[0].responses.create(response="{}", code=SURVEY_CODE)
        response = self.client.post(reverse("survey:survey-detail", args=("survey0", SURVEY_CODE)))
        self.assertRedirects(response, reverse("survey:survey-list"), fetch_redirect_response=False)
        messages = [str(x) for x in get_messages(response.wsgi_request)]
        self.assertListEqual(messages, ["You have already responded to survey Public Survey."])
        self.assertQuerysetEqual(
            Response.objects.all().values_list("survey__slug", "code", "response"),
            [("survey0", SURVEY_CODE, "{}")],
            transform=tuple,
            ordered=False,
        )

    def test_post_response_exists_race(self):
        section = self.surveys[0].sections.create(slug="section0")
        ShortAnswer.objects.create(section=section, slug="q0", required=True)

        orig_post = SurveyDetailView.post

        def post_wrapper(*args, **kwargs):
            self.surveys[0].responses.create(response="{}", code=SURVEY_CODE)
            return orig_post(*args, **kwargs)

        SurveyDetailView.post = post_wrapper

        try:
            response = self.client.post(
                reverse("survey:survey-detail", args=("survey0", SURVEY_CODE)), data={"survey0.section0.q0": "foo"}
            )
        finally:
            SurveyDetailView.post = orig_post

        self.assertContains(response, "Failed to save response because of a duplicate.")
        messages = [str(x) for x in get_messages(response.wsgi_request)]
        self.assertListEqual(messages, ["Failed to save response because of a duplicate."])

        self.assertQuerysetEqual(
            Response.objects.all().values_list("survey__slug", "code", "response"),
            [("survey0", SURVEY_CODE, "{}")],
            transform=tuple,
            ordered=False,
        )


@freeze_time("2000-01-14 12:00:01")
class SurveyEnterViewTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.surveys = [
            Survey.objects.create(
                slug="survey0",
                public=True,
                name="Public Survey",
                start_date=parse_iso_datetime("1999-01-14 12:00:01"),
                report_email=["cherry@example.com", "banana@example.com"],
            ),
            Survey.objects.create(
                slug="survey1",
                public=False,
                name="Private Survey",
                start_date=parse_iso_datetime("1999-01-14 12:00:01"),
            ),
            Survey.objects.create(slug="survey2", public=True, name="Unpublished Survey"),
        ]

    def test_get_not_found(self):
        response = self.client.get(reverse("survey:survey-enter", args=("surveyX",)))
        self.assertEqual(response.status_code, 404)
        self.assertNotIn("survey", response.context)
        self.assertNotIn("form", response.context)

    def test_get_not_started(self):
        response = self.client.get(reverse("survey:survey-enter", args=("survey3",)))
        self.assertEqual(response.status_code, 404)
        self.assertNotIn("survey", response.context)
        self.assertNotIn("form", response.context)

    def test_get(self):
        response = self.client.get(reverse("survey:survey-enter", args=("survey0",)))
        self.assertContains(response, "<title>Survey: Public Survey</title>")
        self.assertEqual(response.context["survey"], self.surveys[0])
        self.assertIsInstance(response.context["form"], SurveyEnterForm)

    def test_post(self):
        response = self.client.post(reverse("survey:survey-enter", args=("survey0",)), data={"key": SURVEY_KEY_MIXED})
        redirect_url = reverse("survey:survey-detail", args=("survey0", SURVEY_CODE))
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)


@freeze_time("2000-01-14 12:00:01")
class SurveySubmittedViewTest(TestCase):
    def setUp(self) -> None:
        self.client = Client()
        self.surveys = [
            Survey.objects.create(
                slug="survey0",
                public=True,
                name="Public Survey",
                start_date=parse_iso_datetime("1999-01-14 12:00:01"),
                report_email=["cherry@example.com", "banana@example.com"],
            ),
            Survey.objects.create(
                slug="survey1",
                public=False,
                name="Private Survey",
                start_date=parse_iso_datetime("1999-01-14 12:00:01"),
            ),
            Survey.objects.create(slug="survey2", public=True, name="Unpublished Survey"),
        ]

        for s in self.surveys:
            s.responses.create(code=SURVEY_CODE)

    def test_get_not_found(self):
        response = self.client.get(reverse("survey:survey-submitted", args=("surveyX", SURVEY_CODE)))
        self.assertEqual(response.status_code, 404)
        self.assertNotIn("survey", response.context)

    def test_get_not_started(self):
        response = self.client.get(reverse("survey:survey-submitted", args=("survey2", SURVEY_CODE)))
        self.assertEqual(response.status_code, 404)
        self.assertNotIn("survey", response.context)

    def test_get_response_found(self):
        response = self.client.get(reverse("survey:survey-submitted", args=("survey0", SURVEY_CODE)))
        self.assertContains(response, "<title>Survey: Public Survey</title>")
        self.assertEqual(response.context["survey"], self.surveys[0])

    def test_get_response_not_found(self):
        response = self.client.get(reverse("survey:survey-submitted", args=("survey0", "XXX")))
        redirect_url = reverse("survey:survey-detail", args=("survey0", "XXX"))
        self.assertRedirects(response, redirect_url, fetch_redirect_response=False)
