import string

from django.test import SimpleTestCase

from survey import utils
from survey.tests.const import (
    SURVEY_CODE,
    SURVEY_CODE_MIXED,
    SURVEY_CODE_UPPER,
    SURVEY_KEY,
    SURVEY_KEY_MIXED,
    SURVEY_KEY_UPPER,
)


class UtilsTest(SimpleTestCase):
    def test_normalize_survey_code_valid(self):
        tc = [
            (SURVEY_CODE, SURVEY_CODE),
            (SURVEY_CODE_UPPER, SURVEY_CODE),
            (SURVEY_CODE_MIXED, SURVEY_CODE),
            (" " + SURVEY_CODE + "\t", SURVEY_CODE),
        ]
        for code, value in tc:
            with self.subTest(code=code):
                self.assertEqual(utils.normalize_survey_code(code), value)

    def test_normalize_survey_code_invalid(self):
        tc = [
            (None, "Code is empty"),
            ("", "Code is empty"),
            (" ", "Code is empty"),
            (
                "🐛",
                "Failed to decode code hash '🐛': 'ascii' codec can't encode character '\\U0001f41b' in position "
                "0: ordinal not in range(128)",
            ),
            (SURVEY_CODE[:9], "Failed to decode code hash '5e50f405a': Odd-length string"),  # noqa: SC300
            (SURVEY_CODE[:16], "Invalid length of hash '5e50f405ace6cbdf': 8"),  # noqa: SC300
        ]
        for code, msg in tc:
            with self.subTest(code=code):
                self.assertRaisesMessage(ValueError, msg, utils.normalize_survey_code, code)

    def test_create_survey_code_valid(self):
        tc = [
            (SURVEY_KEY, SURVEY_CODE),
            (SURVEY_KEY_UPPER, SURVEY_CODE),
            (SURVEY_KEY_MIXED, SURVEY_CODE),
            (" " + SURVEY_KEY + "\t", SURVEY_CODE),
            ("-. m _Yk\tE.y_", SURVEY_CODE),  # noqa: SC300
        ]
        for key, code in tc:
            with self.subTest(key=key):
                self.assertEqual(utils.create_survey_code(key), code)

    def test_create_survey_code_invalid(self):
        tc = [
            (None, "Key is empty"),
            ("", "Key is empty"),
            (" ", "Key is empty"),
            ("🐛my-key", "Key is invalid"),
        ]
        for code, msg in tc:
            with self.subTest(code=code):
                self.assertRaisesMessage(ValueError, msg, utils.create_survey_code, code)

    def test_extract_alphanumeric(self):
        tc = [
            ("", ""),
            (" \t", ""),
            (string.printable, "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"),  # noqa: SC300
            ("🐛 ČUS bug!", "USbug"),  # noqa: SC300
        ]
        for s, res in tc:
            with self.subTest(s):
                self.assertEqual(utils.extract_aplhanumeric(s), res)

    def test_markdownify_links_target_blank_external(self):
        for p in ("http", "https"):
            with self.subTest(protocol=p):
                attrs = utils.markdownify_links_target_blank({(None, "href"): p + "://example.org/"}, False)
                self.assertEqual(attrs[(None, "target")], "_blank")

    def test_markdownify_links_target_blank_internal(self):
        attrs = utils.markdownify_links_target_blank({(None, "href"): "/home/"}, False)
        self.assertNotIn((None, "target"), attrs)
