from django.test import SimpleTestCase

from survey.models import BaseOption, Question, Section, Survey


class QuestionTest(SimpleTestCase):
    def test_str(self):
        self.assertEqual(str(Question(label="First Question")), "First Question")

    def test_str_empty_name(self):
        self.assertEqual(str(Question()), "")

    def test_repr(self):
        survey = Survey(name="First Survey")
        section = Section(name="First Section", survey=survey)
        question = Question(label="First Question", section=section)
        self.assertEqual(repr(question), "<Question: 'First Survey' → 'First Section' → 'First Question'>")

    def test_repr_empty_name(self):
        survey = Survey(name="First Survey")
        section = Section(name="First Section", survey=survey)
        question = Question(section=section)
        self.assertEqual(repr(question), "<Question: 'First Survey' → 'First Section' → ''>")

    def test_repr_empty_section(self):
        question = Question(label="First Question")
        self.assertEqual(repr(question), "<Question: None → None → 'First Question'>")

    def test_repr_empty_survey(self):
        section = Section(name="First Section")
        question = Question(label="First Question", section=section)
        self.assertEqual(repr(question), "<Question: None → 'First Section' → 'First Question'>")


class BaseOptionTest(SimpleTestCase):
    def test_str(self):
        self.assertEqual(str(BaseOption(label="First Option")), "First Option")

    def test_str_empty_name(self):
        self.assertEqual(str(Question()), "")

    def test_repr(self):
        survey = Survey(name="First Survey")
        section = Section(name="First Section", survey=survey)
        question = Question(label="First Question", section=section)
        option = BaseOption(label="First Option", question=question)
        self.assertEqual(
            repr(option), "<BaseOption: 'First Survey' → 'First Section' → 'First Question' → 'First Option'>"
        )

    def test_repr_empty_name(self):
        survey = Survey(name="First Survey")
        section = Section(name="First Section", survey=survey)
        question = Question(label="First Question", section=section)
        option = BaseOption(question=question)
        self.assertEqual(repr(option), "<BaseOption: 'First Survey' → 'First Section' → 'First Question' → ''>")

    def test_repr_empty_survey(self):
        section = Section(name="First Section")
        question = Question(label="First Question", section=section)
        option = BaseOption(label="First Option", question=question)
        self.assertEqual(repr(option), "<BaseOption: None → 'First Section' → 'First Question' → 'First Option'>")

    def test_repr_empty_section(self):
        question = Question(label="First Question")
        option = BaseOption(label="First Option", question=question)
        self.assertEqual(repr(option), "<BaseOption: None → None → 'First Question' → 'First Option'>")

    def test_repr_empty_question(self):
        option = BaseOption(label="First Option")
        self.assertEqual(repr(option), "<BaseOption: None → None → None → 'First Option'>")
