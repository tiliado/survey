from django.core.exceptions import ValidationError
from django.test import SimpleTestCase, TestCase
from freezegun.api import freeze_time

from survey.models import Response, Section, Survey
from survey.tests.utils import parse_iso_datetime


class SurveyTest(TestCase):
    DATES = [
        (None, None),  # 0: no start date
        (None, "2012-01-14 12:00:10"),  # 1: no start date
        ("2012-01-14 12:00:01", None),  # 2: now > start_date
        ("2012-01-14 12:00:02", None),  # 3: now == start_date
        ("2012-01-14 12:00:03", None),  # 4: now < start_date
        ("2012-01-14 12:00:00", "2012-01-14 12:00:01"),  # 5: now > end_date
        ("2012-01-14 12:00:00", "2012-01-14 12:00:02"),  # 6: now == end_date
        ("2012-01-14 12:00:00", "2012-01-14 12:00:03"),  # 7: now < end_date
    ]

    def create_surveys(self):
        for i, (start, end) in enumerate(self.DATES):
            Survey.objects.create(
                pk=i, slug=str(i), start_date=parse_iso_datetime(start), end_date=parse_iso_datetime(end)
            )

    @freeze_time("2012-01-14 12:00:02")
    def test_filter_open(self):
        self.create_surveys()
        qs = Survey.filter_open(Survey.objects.order_by("pk"))
        self.assertQuerysetEqual(
            qs.values_list("pk", flat=True),
            (2, 3, 7),
            transform=int,
        )

    @freeze_time("2012-01-14 12:00:02")
    def test_filter_closed(self):
        self.create_surveys()
        qs = Survey.filter_closed(Survey.objects.order_by("pk"))
        self.assertQuerysetEqual(
            qs.values_list("pk", flat=True),
            (0, 1, 4, 5, 6),
            transform=int,
        )

    @freeze_time("2012-01-14 12:00:02")
    def test_is_open(self):
        results = [False, False, True, True, False, False, False, True]

        for (start, end), result in zip(self.DATES, results):
            with self.subTest(start=start, end=end):
                survey = Survey(start_date=parse_iso_datetime(start), end_date=parse_iso_datetime(end))
                self.assertIs(survey.is_open, result)

    def test_clean_start_and_end_date_ok(self):
        data = [
            (None, None),
            (None, "2012-01-14 12:00:00"),
            ("2012-01-14 12:00:00", None),
            ("2012-01-14 12:00:00", "2012-01-14 12:00:01"),
        ]

        for start, end in data:
            with self.subTest(start=start, end=end):
                survey = Survey(start_date=parse_iso_datetime(start), end_date=parse_iso_datetime(end))
                survey.clean()

    def test_clean_start_and_end_date_fail(self):
        data = [
            ("2012-01-14 12:00:01", "2012-01-14 12:00:01"),
            ("2012-01-14 12:00:01", "2012-01-14 12:00:00"),
        ]
        msg = "{'end_date': ['Survey cannot end before its beginning.']}"

        for start, end in data:
            with self.subTest(start=start, end=end):
                survey = Survey(start_date=parse_iso_datetime(start), end_date=parse_iso_datetime(end))
                self.assertRaisesMessage(ValidationError, msg, survey.clean)

    def test_str(self):
        self.assertEqual(str(Survey(name="User Survey")), "User Survey")

    def test_str_empty_name(self):
        self.assertEqual(str(Survey()), "")

    def test_repr(self):
        self.assertEqual(repr(Survey(name="User Survey")), "<Survey: 'User Survey'>")

    def test_repr_empty_name(self):
        self.assertEqual(repr(Survey()), "<Survey: ''>")


class SectionTest(TestCase):
    def test_str(self):
        self.assertEqual(str(Section(name="First Section")), "First Section")

    def test_str_empty_name(self):
        self.assertEqual(str(Section()), "")

    def test_repr(self):
        self.assertEqual(
            repr(Section(name="First Section", survey=Survey(name="First Survey"))),
            "<Section: 'First Survey' → 'First Section'>",
        )

    def test_repr_empty_name(self):
        self.assertEqual(repr(Section(survey=Survey(name="First Survey"))), "<Section: 'First Survey' → ''>")

    def test_repr_empty_survey(self):
        self.assertEqual(
            repr(Section(name="First Section")),
            "<Section: No survey → 'First Section'>",
        )


class ResponseTest(SimpleTestCase):
    def test_str_with_survey(self):
        s = Survey(name="First Survey")
        r = Response(survey=s, code="beef")
        self.assertEqual(str(r), "First Survey: beef")

    def test_str_without_survey(self):
        r = Response(code="beef")
        self.assertEqual(str(r), "beef")

    def test_repr_with_survey(self):
        s = Survey(name="First Survey")
        r = Response(survey=s, code="beef")
        self.assertEqual(repr(r), "<Response: 'First Survey' → beef>")

    def test_repr_without_survey(self):
        r = Response(code="beef")
        self.assertEqual(repr(r), "<Response: No survey → beef>")
