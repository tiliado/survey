"""Survey and Section models."""
from django.core.exceptions import ObjectDoesNotExist, ValidationError
from django.db.models import (
    CASCADE,
    BooleanField,
    CharField,
    DateTimeField,
    ForeignKey,
    Model,
    PositiveIntegerField,
    Q,
    QuerySet,
    SlugField,
    TextField,
    UniqueConstraint,
)
from django.utils import timezone
from markdownx.models import MarkdownxField
from multi_email_field.fields import MultiEmailField


class Survey(Model):
    """Survey model."""

    name = CharField(max_length=100, help_text="Human readable name of the survey.")
    slug = SlugField(max_length=100, unique=True, help_text="Identifier shown in URLs.")
    description = MarkdownxField(blank=True)
    text_enter = MarkdownxField(blank=True)
    text_submitted = MarkdownxField(blank=True)
    public = BooleanField(default=False, help_text="Whether the survey is show in public survey list.")
    start_date = DateTimeField(blank=True, null=True, help_text="If no start date is provided, the survey is closed.")
    end_date = DateTimeField(blank=True, null=True, help_text="If no end date is provided, the survey remains open.")
    report_email = MultiEmailField(blank=True, null=True, help_text="E-mails to send survey results to.")

    @staticmethod
    def filter_open(queryset: QuerySet) -> QuerySet:
        """Filter queryset to contain only open surveys."""
        now = timezone.now()
        return queryset.exclude(start_date=None).filter(start_date__lte=now).exclude(end_date__lte=now)

    @staticmethod
    def filter_closed(queryset: QuerySet) -> QuerySet:
        """Filter queryset to contain only closed surveys."""
        now = timezone.now()
        return queryset.filter(Q(end_date__lte=now) | Q(start_date=None) | Q(start_date__gt=now))

    @property
    def is_open(self) -> bool:
        """Whether the survey is open."""
        now = timezone.now()
        return self.start_date is not None and self.start_date <= now and (not self.end_date or self.end_date > now)

    def clean(self) -> None:
        """Validate multiple fields."""
        if self.start_date and self.end_date and self.start_date >= self.end_date:
            raise ValidationError({"end_date": "Survey cannot end before its beginning."})

    def __str__(self) -> str:
        return self.name or ""

    def __repr__(self) -> str:
        return f"<{type(self).__name__}: {self.name!r}>"


class Section(Model):
    """A model of a survey section."""

    survey = ForeignKey(Survey, on_delete=CASCADE, db_index=True, related_name="sections")
    name = CharField(max_length=100, help_text="Human readable name of the section.")
    position = PositiveIntegerField(null=True, blank=True)
    slug = SlugField(max_length=100)
    description = MarkdownxField(blank=True)

    class Meta:
        """Meta attributes."""

        ordering = ["position", "pk"]
        constraints = [UniqueConstraint(fields=["survey", "slug"], name="%(app_label)s_%(class)s_1")]

    def __str__(self) -> str:
        return self.name or ""

    def __repr__(self) -> str:
        try:
            survey = self.survey
        except ObjectDoesNotExist:
            return f"<{type(self).__name__}: No survey → {self.name!r}>"
        else:
            return f"<{type(self).__name__}: {survey.name!r} → {self.name!r}>"


class Response(Model):
    """A model to hold survey responses."""

    survey = ForeignKey(Survey, on_delete=CASCADE, db_index=True, related_name="responses")
    code = CharField(max_length=100, db_index=True)
    created = DateTimeField(auto_now_add=True)
    response = TextField()

    class Meta:
        """Meta attributes."""

        ordering = ["-created", "pk"]
        constraints = [UniqueConstraint(fields=["survey", "code"], name="%(app_label)s_%(class)s_1")]

    def __str__(self) -> str:
        try:
            survey = self.survey
        except ObjectDoesNotExist:
            return str(self.code)
        else:
            return f"{survey.name}: {self.code}"

    def __repr__(self) -> str:
        try:
            survey = self.survey
        except ObjectDoesNotExist:
            return f"<{type(self).__name__}: No survey → {self.code}>"
        else:
            return f"<{type(self).__name__}: {survey.name!r} → {self.code}>"
