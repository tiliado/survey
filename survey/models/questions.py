"""Question models."""
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import (
    CASCADE,
    BooleanField,
    CharField,
    ForeignKey,
    Model,
    PositiveIntegerField,
    SlugField,
    TextField,
    UniqueConstraint,
    URLField,
)
from markdownx.models import MarkdownxField
from polymorphic.models import PolymorphicModel

from survey.models.survey import Section


class Question(PolymorphicModel):
    """Base class for questions."""

    section = ForeignKey(Section, on_delete=CASCADE, db_index=True, related_name="questions")
    label = CharField(max_length=255)
    position = PositiveIntegerField(null=True, blank=True)
    slug = SlugField(max_length=255)
    description = MarkdownxField(blank=True)
    required = BooleanField(default=True)

    class Meta:
        """Meta attributes."""

        ordering = ["position", "pk"]
        constraints = [UniqueConstraint(fields=["section", "slug"], name="%(app_label)s_%(class)s_1")]

    def __str__(self) -> str:
        return self.label or ""

    def __repr__(self) -> str:
        try:
            section = self.section.name
            try:
                survey = self.section.survey.name
            except ObjectDoesNotExist:
                survey = None
        except ObjectDoesNotExist:
            survey = section = None

        return f"<{type(self).__name__}: {survey!r} → {section!r} → {self.label!r}>"


class ShortAnswer(Question):
    """Question with a short answer."""

    max_length = PositiveIntegerField(default=255)


class LongAnswer(Question):
    """Question with a long answer."""

    max_length = PositiveIntegerField(default=2048)


class MultipleChoice(Question):
    """Question with multiple choices."""

    selection_limit = PositiveIntegerField(blank=True, null=True)
    other_value = BooleanField(default=False)


class ChoiceGrid(Question):
    """Question with multiple entries and multiple choices."""

    selection_limit = PositiveIntegerField(blank=True, null=True)


class BaseOption(Model):
    """Base model for options and entries."""

    question = ForeignKey(Question, on_delete=CASCADE, db_index=True, related_name="options")
    label = CharField(max_length=255)
    position = PositiveIntegerField(null=True, blank=True)
    slug = SlugField(max_length=255)
    link = URLField(blank=True, null=True)
    description = TextField(blank=True)

    class Meta:
        """Meta attributes."""

        abstract = True
        ordering = ["position", "pk"]
        constraints = [UniqueConstraint(fields=["question", "slug"], name="%(app_label)s_%(class)s_1")]

    def __str__(self) -> str:
        return self.label or ""

    def __repr__(self) -> str:
        try:
            q = self.question
            question = q.label
            try:
                s = q.section
                section = s.name
                try:
                    survey = s.survey.name
                except ObjectDoesNotExist:
                    survey = None
            except ObjectDoesNotExist:
                survey = section = None
        except ObjectDoesNotExist:
            survey = section = question = None

        return f"<{type(self).__name__}: {survey!r} → {section!r} → {question!r} → {self.label!r}>"


class Option(BaseOption):
    """Options for multiple choice and choice grid questions."""

    selected = BooleanField(default=False)


class Entry(BaseOption):
    """Entry in choice grid question."""

    question = ForeignKey(Question, on_delete=CASCADE, db_index=True, related_name="entries")
