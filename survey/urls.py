"""URL configuration of Survey application."""
from django.urls import path

from survey.views import SurveyDetailView, SurveyEnterView, SurveyListView, SurveySubmittedView

app_name = "survey"
urlpatterns = [
    path("", SurveyListView.as_view(), name="survey-list"),
    path("<slug:slug>/", SurveyEnterView.as_view(), name="survey-enter"),
    path("<slug:slug>/<str:code>/", SurveyDetailView.as_view(), name="survey-detail"),
    path("<slug:slug>/<str:code>/submitted/", SurveySubmittedView.as_view(), name="survey-submitted"),
]
