"""Survey form and section rendering."""
from __future__ import annotations

from datetime import datetime
from typing import Any, Optional

from django.http import QueryDict

from survey.forms.base import FormWidget, Renderable
from survey.models import Section, Survey


class SurveyForm(Renderable):
    """Survey form validation and rendering."""

    template_name = "survey/form/form.html"
    model: Survey
    data: Optional[QueryDict]
    data_key: str
    sections: dict[str, FormSection]
    initial: dict[str, Any]
    expiration_date: Optional[datetime]

    def __init__(
        self,
        model: Survey,
        *,
        prefix: str = None,
        data: QueryDict = None,
        initial: dict[str, Any] = None,
        expiration_date: datetime = None,
    ):
        if not isinstance(initial, dict):
            initial = {}

        self.model = model
        self.data = data
        self.data_key = f"{prefix}.{model.slug}" if prefix else model.slug
        self.initial = initial
        self.expiration_date = expiration_date
        self.sections = {
            x.slug: FormSection(x, f"{self.data_key}.{x.slug}", data, initial.get(x.slug)) for x in model.sections.all()
        }

    def is_valid(self) -> bool:
        """Get result of validation."""
        all_results = [x.is_valid() for x in self.sections.values()]
        return all(all_results)

    def has_error(self) -> bool:
        """Check whether this widget has any error."""
        return any(x.has_error() for x in self.sections.values())

    def get_cleaned_data(self) -> dict[str, Any]:
        """Return cleaned data upon validation."""
        return {name: x.get_cleaned_data() for name, x in self.sections.items()}

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Get context data for template rendering."""
        context = super().get_context_data(**kwargs)
        context["has_error"] = self.has_error()
        context["survey"] = self.model
        context["sections"] = self.sections
        context["data_key"] = self.data_key
        context["expiration_date"] = self.expiration_date
        return context

    def serialize_data(self) -> Optional[dict[str, Any]]:
        """Serialize responses to the survey."""
        if self.data is None:
            return None
        return {slug: section.serialize_data() for slug, section in self.sections.items()}

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__}: {self.model!r}>"


class FormSection(Renderable):
    """Survey form section validation and rendering."""

    template_name: str = "survey/form/section.html"
    model: Section
    data: Optional[QueryDict]
    data_key: str

    def __init__(self, model: Section, data_key: str, data: Optional[QueryDict], initial: Optional[dict[str, Any]]):
        if initial is None:
            initial = {}

        self.model = model
        self.data_key = data_key
        self.data = data
        self.initial = initial
        self.questions = {
            x.slug: FormWidget.create_for_model(x, f"{data_key}.{x.slug}", data, initial.get(x.slug))
            for x in model.questions.all()
        }

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Get context data for template rendering."""
        context = super().get_context_data(**kwargs)
        context["section"] = self.model
        context["questions"] = self.questions
        context["data_key"] = self.data_key
        context["has_error"] = self.has_error()
        return context

    def serialize_data(self) -> Optional[dict[str, Any]]:
        """Serialize responses to the section."""
        if self.data is None:
            return None

        return {slug: question.serialize_data() for slug, question in self.questions.items()}

    def is_valid(self) -> bool:
        """Get result of validation."""
        all_results = [x.is_valid() for x in self.questions.values()]
        return all(all_results)

    def has_error(self) -> bool:
        """Check whether this widget has any error."""
        return any(x.has_error() for x in self.questions.values())

    def get_cleaned_data(self) -> dict[str, Any]:
        """Return cleaned data upon validation."""
        return {name: x.get_cleaned_data() for name, x in self.questions.items()}

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__}: {self.model!r}>"
