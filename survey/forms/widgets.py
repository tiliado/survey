"""Widgets to manage individual question types."""
from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Any, Generic, Optional, TypeVar, Union, cast

from django.core.exceptions import ValidationError
from django.http import QueryDict

from survey.forms import FormWidget
from survey.forms.base import Q
from survey.models import ChoiceGrid, Entry, LongAnswer, MultipleChoice, Option, ShortAnswer

D = TypeVar("D")


class ValidationMixin(Generic[D], ABC):
    """A mixin to perform validation and store cleaned data or validation error."""

    valid: Optional[bool] = None
    error: Optional[ValidationError] = None
    cleaned_data: Optional[D] = None

    def is_valid(self) -> bool:
        """Get result of validation."""
        if self.valid is None:
            try:
                self.cleaned_data = self.validate()
                self.valid = True
            except ValidationError as e:
                self.error = e
                self.valid = False
        return self.valid

    @abstractmethod
    def validate(self) -> D:
        """Validate raw data and return cleaned data."""


class TextAnswerWidget(ValidationMixin[str], FormWidget[Q]):
    """Validation and rendering of ShortAnswer."""

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Get context data for template rendering."""
        context = super().get_context_data(**kwargs)
        context["value"] = self.value
        return context

    @property
    def value(self) -> str:
        """Get current value."""
        if self.data is not None:
            return cast(str, self.data.get(self.data_key, ""))
        if isinstance(self.initial, list) and self.initial:
            return self.initial[-1]
        return ""

    def get_cleaned_data(self) -> Any:
        """Return cleaned data upon validation or None if there was no validation yet."""
        return self.cleaned_data

    def validate(self) -> str:
        """Validate raw data and return cleaned data."""
        raw_data = cast(Optional[list[str]], self.serialize_data())
        if raw_data is None:
            data = ""
        else:
            data = raw_data[-1] if raw_data else ""

        if not data and self.model.required:
            raise ValidationError("This answer is required.")
        if len(data) > self.model.max_length:
            raise ValidationError("This answer is too long.")
        return data

    def has_error(self) -> bool:
        """Check whether this widget has any error."""
        return bool(self.error)


class ShortAnswerWidget(TextAnswerWidget[ShortAnswer]):
    """Validation and rendering of ShortAnswer."""

    model_type = ShortAnswer


class LongAnswerWidget(TextAnswerWidget[LongAnswer]):
    """Validation and rendering of LongAnswer."""

    model_type = LongAnswer


class MultipleChoiceWidget(FormWidget[MultipleChoice]):
    """Validation and rendering of MultipleChoice."""

    model_type = MultipleChoice
    options: list[Option]
    choice: Choice

    def __init__(
        self,
        model: MultipleChoice,
        data_key: str,
        data: Optional[QueryDict],
        initial: Optional[Union[list[str], dict[str, Any]]],
    ):
        options = list(model.options.all())

        if initial is None or isinstance(initial, dict):
            initial = [x.slug for x in options if x.selected]

        super().__init__(model, data_key, data, initial)
        self.options = options
        self.choice = Choice(
            data_key, data, initial, {x.slug for x in self.options}, model.selection_limit, model.required
        )

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Get context data for template rendering."""
        context = super().get_context_data(**kwargs)
        context["values"] = self.choice.values
        context["options"] = self.options
        context["choice"] = self.choice
        return context

    def is_valid(self) -> bool:
        """Get result of validation."""
        return self.choice.is_valid()

    def has_error(self) -> bool:
        """Check whether this widget has any error."""
        return bool(self.choice.error)

    def get_cleaned_data(self) -> Any:
        """Return cleaned data upon validation or None if there was no validation yet."""
        return self.choice.cleaned_data


class ChoiceGridWidget(FormWidget[ChoiceGrid]):
    """Validation and rendering of ChoiceGrid."""

    model_type = ChoiceGrid
    options: list[Option]
    choices: dict[str, GridChoice]

    def __init__(self, model: ChoiceGrid, data_key: str, data: Optional[QueryDict], initial: Optional[dict[str, Any]]):
        super().__init__(model, data_key, data, initial)

        if initial is None:
            initial = {}

        self.options = list(model.options.all())
        options = {x.slug for x in self.options}

        self.choices = {}
        for entry in model.entries.all():
            entry_initial = initial.get(entry.slug)
            if entry_initial is None:
                entry_initial = [x.slug for x in self.options if x.selected]

            self.choices[entry.slug] = GridChoice(
                entry, f"{data_key}.{entry.slug}", data, entry_initial, options, model.selection_limit, model.required
            )

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Get context data for template rendering."""
        context = super().get_context_data(**kwargs)
        context["choices"] = self.choices
        context["options"] = self.options
        return context

    def serialize_data(self) -> Optional[dict[str, Any]]:
        """Serialize responses to the question."""
        if self.data is None:
            return None
        return {slug: entry.serialize_data() for slug, entry in self.choices.items()}

    def is_valid(self) -> bool:
        """Get result of validation."""
        all_results = [x.is_valid() for x in self.choices.values()]
        return all(all_results)

    def has_error(self) -> bool:
        """Check whether this widget has any error."""
        return any(x.error for x in self.choices.values())

    def get_cleaned_data(self) -> Optional[Union[list[str], dict[str, Any]]]:
        """Return cleaned data upon validation or None if there was no validation yet."""
        return {name: entry.cleaned_data for name, entry in self.choices.items()}


class Choice(ValidationMixin[list[str]]):
    """Validation of a MultipleChoice entry."""

    data: Optional[QueryDict]
    data_key: str

    def __init__(
        self,
        data_key: str,
        data: Optional[QueryDict],
        initial: Optional[list[str]],
        options: set[str],
        limit: int,
        required: bool,
    ):
        if initial is None:
            initial = []

        self.data_key = data_key
        self.data = data
        self.initial = initial
        self.options = options
        self.limit = limit
        self.required = required

    def serialize_data(self) -> Optional[list[str]]:
        """Serialize entry values."""
        if self.data is None:
            return None
        return cast(list[str], self.data.getlist(self.data_key))

    def validate(self) -> list[str]:
        """Validate raw data and return cleaned data."""
        data = set(self.serialize_data() or [])
        extra = data - self.options
        if extra:
            raise ValidationError("Invalid values: %(values)r.", params={"values": sorted(extra)})

        if self.limit and len(data) > self.limit:
            raise ValidationError("Too many options have been selected.")

        if self.required and not data:
            raise ValidationError("This answer is required.")

        return sorted(data)

    @property
    def values(self) -> list[str]:
        """Get current values."""
        if self.data is None:
            return self.initial or []
        return cast(list[str], self.data.getlist(self.data_key))


class GridChoice(Choice):
    """Validation of a ChoiceGrid entry."""

    model: Entry

    def __init__(self, model: Entry, *args: Any, **kwargs: Any):
        super().__init__(*args, **kwargs)
        self.model = model
