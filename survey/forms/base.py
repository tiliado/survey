"""Base classes for survey form rendering."""
from __future__ import annotations

import re
import typing
from abc import ABC, abstractmethod
from typing import Any, ClassVar, Generic, Optional, Type, TypeVar, Union, cast

from django.core.exceptions import ImproperlyConfigured
from django.http import QueryDict
from django.template.loader import get_template
from django.utils.safestring import SafeString

from survey.models import Question


class Renderable:
    """Renderable object."""

    template_name: Optional[str] = None

    def get_template_name(self) -> str:
        """Get template name to render this object."""
        if not self.template_name:
            raise ImproperlyConfigured(f"No template provided for {type(self).__name__}.")
        return self.template_name

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Get context data for template rendering."""
        return kwargs

    def render(self, **kwargs: Any) -> SafeString:
        """Render template."""
        template = get_template(self.get_template_name())
        context = self.get_context_data(**kwargs)
        return template.render(context)

    def __str__(self) -> str:
        return str(self.render())

    def __repr__(self) -> str:
        return f"<{type(self).__name__}>"


Q = TypeVar("Q", bound=Question)


class FormWidget(Generic[Q], Renderable, ABC):
    """Widget to render and validate questions."""

    factories: dict[typing.Type[Question], typing.Type[FormWidget[Q]]] = {}
    model: Q
    data: Optional[QueryDict]
    data_key: str
    initial: Optional[Union[list[str], dict[str, Any]]]
    model_type: ClassVar[Optional[Type[Question]]] = None

    @classmethod
    def __init_subclass__(cls) -> None:
        super().__init_subclass__()

        model_type = getattr(cls, "model_type", None)
        if model_type:
            cls.factories[model_type] = cls

    @classmethod
    def create_for_model(cls, model: Q, *args: Any, **kwargs: Any) -> FormWidget[Q]:
        """Create a widget to render and validate the given model."""
        try:
            factory = cls.factories[type(model)]
        except KeyError:
            raise TypeError(f"No widget found for {type(model)}.") from None
        else:
            return factory(model, *args, **kwargs)

    def __init__(
        self, model: Q, data_key: str, data: Optional[QueryDict], initial: Optional[Union[list[str], dict[str, Any]]]
    ):
        self.model = model
        self.data_key = data_key
        self.data = data
        self.initial = initial

    def get_template_name(self) -> str:
        """Get template name to render this widget."""
        if self.template_name:
            return self.template_name
        name = re.sub("(?!^)([A-Z]+)", r"_\1", type(self.model).__name__).lower()
        return f"survey/question/{name}.html"

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Get context data for template rendering."""
        context = super().get_context_data(**kwargs)
        context["widget"] = self
        context["question"] = self.model
        context["data_key"] = self.data_key
        return context

    def serialize_data(self) -> Optional[Union[list[str], dict[str, Any]]]:
        """Serialize responses to the question."""
        if self.data is None:
            return None
        return cast(list[str], self.data.getlist(self.data_key))

    @abstractmethod
    def is_valid(self) -> bool:
        """Get result of validation."""

    @abstractmethod
    def get_cleaned_data(self) -> Any:
        """Return cleaned data upon validation or None if there was no validation yet."""

    @abstractmethod
    def has_error(self) -> bool:
        """Check whether this widget has any error."""

    def __repr__(self) -> str:
        return f"<{self.__class__.__name__}: {self.model!r}>"
