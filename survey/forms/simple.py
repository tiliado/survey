"""Simple utility forms for Survey app."""
from django.core.exceptions import ValidationError
from django.forms import CharField, Form
from django.utils.translation import gettext_lazy as _

from survey import utils


def validate_survey_key(value: str) -> None:
    """Validate survey key."""
    try:
        utils.normalize_survey_key(value)
    except ValueError:
        raise ValidationError(_("Key is invalid."))


class SurveyEnterForm(Form):
    """Request a key to enter the survey."""

    key = CharField(label="Survey key", max_length=255, validators=[validate_survey_key])
