"""Response administration."""
from typing import cast

from django.contrib import admin
from django.http.request import HttpRequest
from django.utils.html import format_html

from survey.admin.base import ModelAdmin
from survey.models import Response


@admin.register(Response)
class ResponseAdmin(ModelAdmin):
    """Model admin for Response model."""

    date_hierarchy = "created"
    list_select_related = ("survey",)
    search_fields = ["code"]
    list_display = list_display_links = ("survey", "code", "created")
    list_filter = (("survey", admin.RelatedOnlyFieldListFilter), "created")
    fields = ("survey", "code_verbatim", "response_verbatim")

    def has_add_permission(self, request: HttpRequest) -> bool:
        """Disable adding responses."""
        return False

    def has_change_permission(self, request: HttpRequest, obj: Response = None) -> bool:
        """Disable changing responses."""
        return False

    def code_verbatim(self, obj: Response) -> str:
        """Render code with mono-space font."""
        return cast(str, format_html("<code>{}</code>", obj.code))

    code_verbatim.short_description = "Code"  # type: ignore

    def response_verbatim(self, obj: Response) -> str:
        """Render response verbatim preserving whitespace."""
        return cast(str, format_html("<pre>{}</pre>", obj.response))

    response_verbatim.short_description = "Response"  # type: ignore
