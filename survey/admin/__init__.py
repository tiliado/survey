"""Django admin declarations of Survey application."""
from survey.admin.response import ResponseAdmin
from survey.admin.survey import SurveyAdmin

__all__ = ["SurveyAdmin", "ResponseAdmin"]
