"""Django admin declarations for Survey and Section models."""
from django.contrib import admin
from django.db.models import QuerySet
from django.http import HttpRequest
from nested_admin.nested import NestedStackedInline, NestedTabularInline
from nested_admin.polymorphic import NestedStackedPolymorphicInline

from survey.admin.base import ModelAdmin
from survey.models import ChoiceGrid, Entry, LongAnswer, MultipleChoice, Option, Question, Section, ShortAnswer, Survey


class EntryInline(NestedTabularInline):
    """Inline admin for Entry model."""

    model = Entry
    sortable_field_name = "position"
    extra = 0
    fields = ["label", "slug", "position", "link"]


class OptionInline(NestedTabularInline):
    """Inline admin for Option model."""

    model = Option
    sortable_field_name = "position"
    extra = 0
    fields = ["label", "slug", "position", "link", "selected"]


class QuestionInline(NestedStackedPolymorphicInline):
    """Inline admin for Question models."""

    class ShortAnswerInline(NestedStackedPolymorphicInline.Child):
        model = ShortAnswer
        fieldsets = [
            (
                None,
                {"fields": (("label", "slug"),)},
            ),
            (
                "Details",
                {
                    "classes": ("collapse",),
                    "fields": (("position", "required", "max_length"), "description"),
                },
            ),
        ]

    class LongAnswerInline(NestedStackedPolymorphicInline.Child):
        model = LongAnswer
        fieldsets = [
            (
                None,
                {"fields": (("label", "slug"),)},
            ),
            (
                "Details",
                {
                    "classes": ("collapse",),
                    "fields": (("position", "required", "max_length"), "description"),
                },
            ),
        ]

    class MultipleChoiceInline(NestedStackedPolymorphicInline.Child):
        model = MultipleChoice
        inlines = [OptionInline]
        fieldsets = [
            (
                None,
                {"fields": (("label", "slug"),)},
            ),
            (
                "Details",
                {
                    "classes": ("collapse",),
                    "fields": (("position", "required", "other_value", "selection_limit"), "description"),
                },
            ),
        ]

    class ChoiceGridInline(NestedStackedPolymorphicInline.Child):
        model = ChoiceGrid
        inlines = [OptionInline, EntryInline]
        fieldsets = [
            (
                None,
                {"fields": (("label", "slug"),)},
            ),
            (
                "Details",
                {
                    "classes": ("collapse",),
                    "fields": (("position", "required", "selection_limit"), "description"),
                },
            ),
        ]

    model = Question
    child_inlines = [ShortAnswerInline, LongAnswerInline, MultipleChoiceInline, ChoiceGridInline]
    sortable_field_name = "position"


class SectionInline(NestedStackedInline):
    """Inline admin for Section model."""

    model = Section
    sortable_field_name = "position"
    extra = 0
    inlines = [QuestionInline]
    fieldsets = [
        (
            None,
            {"fields": (("name", "slug", "position"),)},
        ),
        (
            "Details",
            {
                "classes": ("collapse",),
                "fields": ("description",),
            },
        ),
    ]


class SurveyOpenFilter(admin.SimpleListFilter):
    """Survey filter based on start and end date."""

    title = "open"
    parameter_name = "is_open"

    def lookups(self, request: HttpRequest, model_admin: admin.ModelAdmin) -> list[tuple[str, str]]:
        """Return look-up values and labels."""
        return [
            ("true", "Open"),
            ("false", "Closed"),
        ]

    def queryset(self, request: HttpRequest, queryset: QuerySet) -> QuerySet:
        """Return the filtered queryset."""
        value = self.value()
        if value == "true":
            return Survey.filter_open(queryset)
        if value == "false":
            return Survey.filter_closed(queryset)
        return queryset


@admin.register(Survey)
class SurveyAdmin(ModelAdmin):
    """Admin for Survey model."""

    prepopulated_fields = {"slug": ("name",)}
    save_as = True
    date_hierarchy = "start_date"
    list_display = ("name", "slug", "public", "is_open", "start_date", "end_date")
    list_filter = (SurveyOpenFilter, "public", "start_date", "end_date")
    inlines = [SectionInline]
    fieldsets = [
        (
            None,
            {"fields": (("name", "slug"), "public", ("start_date", "end_date"))},
        ),
        (
            "Details",
            {
                "classes": ("collapse",),
                "fields": (
                    "report_email",
                    "description",
                    "text_enter",
                    "text_submitted",
                ),
            },
        ),
    ]

    def is_open(self, obj: Survey) -> bool:
        """Whether the survey is open or closed."""
        return obj.is_open

    is_open.short_description = "Open"  # type: ignore
    is_open.boolean = True  # type: ignore
