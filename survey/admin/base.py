"""Base Django admin declarations."""
from markdownx.admin import MarkdownxModelAdmin
from nested_admin.polymorphic import NestedPolymorphicModelAdmin


class ModelAdmin(MarkdownxModelAdmin, NestedPolymorphicModelAdmin):
    """Customized model admin."""

    actions_on_bottom = True
    save_on_top = True

    class Media:
        css = {"all": ("survey/css/markdown.css",)}
