"""Views of Survey application."""
import json
import logging
from typing import Any, Optional, cast

from django.contrib import messages
from django.core.mail import send_mail
from django.db import transaction
from django.db.models.query import QuerySet
from django.db.utils import IntegrityError
from django.http.request import HttpRequest, QueryDict
from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import FormView, ListView
from django.views.generic.base import TemplateView

from survey import utils
from survey.forms import SurveyEnterForm, SurveyForm
from survey.models import Survey

LOGGER = logging.getLogger(__name__)


class SurveyListView(ListView):
    """List of public open surveys."""

    model = Survey
    paginate_by = 10
    allow_empty = True
    ordering = ["-start_date"]

    def get_queryset(self) -> QuerySet:
        """Return filtered queryset."""
        return Survey.filter_open(super().get_queryset()).filter(public=True)


class SurveyDetailView(TemplateView):
    """Render and process a survey."""

    code: str
    survey: Survey
    form: SurveyForm
    data: Optional[QueryDict] = None
    form_prefix: Optional[str] = None
    session_prefix: Optional[str] = "survey"
    template_name = "survey/survey_detail.html"

    def dispatch(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        """Dispatch HTTP request."""
        try:
            self.code = utils.normalize_survey_code(self.kwargs.get("code"))
        except ValueError:
            LOGGER.warning("Invalid survey code: %r.", self.kwargs.get("code"), exc_info=True)
            return HttpResponseRedirect(reverse("survey:survey-enter", kwargs={"slug": self.kwargs["slug"]}))

        self.survey = self.get_survey()

        if self.survey.responses.filter(code=self.code).exists():
            messages.error(request, f"You have already responded to survey {self.survey.name}.")
            return HttpResponseRedirect(reverse("survey:survey-list"))

        return super().dispatch(request, *args, **kwargs)

    def get_survey(self) -> Survey:
        """Return open survey by slug."""
        return cast(
            Survey,
            get_object_or_404(
                Survey.filter_open(Survey.objects.all()).prefetch_related("sections"), slug=self.kwargs["slug"]
            ),
        )

    def get_form(self, *, data: QueryDict = None, initial: dict[str, Any] = None) -> SurveyForm:
        """Get survey form."""
        return SurveyForm(
            self.survey,
            data=data,
            initial=initial,
            prefix=self.form_prefix,
            expiration_date=self.request.session.get_expiry_date(),
        )

    def get_session_key(self) -> str:
        """Get session key for survey data."""
        if self.session_prefix:
            return f"{self.session_prefix}.{self.survey.slug}"
        return cast(str, self.survey.slug)

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Get template context data."""
        context: dict[str, Any] = super().get_context_data(**kwargs)
        context["form"] = self.form
        context["survey"] = self.survey
        return context

    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        """Handle GET requests: instantiate a blank form."""
        self.form = self.get_form(initial=self.request.session.get(self.get_session_key()))
        return super().get(request, *args, **kwargs)

    def post(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        """Handle POST requests: instantiate a blank form and save POST data to session."""
        self.form = self.get_form(data=self.request.POST)
        self.request.session[self.get_session_key()] = self.form.serialize_data()
        self.form.expiration_date = self.request.session.get_expiry_date()

        if self.request.POST.get("save"):
            return self.render_to_response(self.get_context_data(**kwargs))

        if self.form.is_valid():
            return self.form_valid(**kwargs)
        return self.form_invalid(**kwargs)

    def form_valid(self, **kwargs: Any) -> HttpResponse:
        """Handle valid form."""
        data = self.form.get_cleaned_data()

        try:
            with transaction.atomic():
                self.survey.responses.create(
                    code=self.code, response=json.dumps(data, sort_keys=True, indent=2, ensure_ascii=False)
                )
        except IntegrityError:
            messages.error(self.request, "Failed to save response because of a duplicate.")
            return self.render_to_response(self.get_context_data(**kwargs))

        if self.survey.report_email:
            msg = json.dumps({"code": self.code, "data": data}, sort_keys=True, indent=2, ensure_ascii=False)

            for email in self.survey.report_email:
                try:
                    send_mail(f"New survey result for {self.survey.name}", msg, None, [email])
                except Exception:
                    LOGGER.exception("Failed to send survey %r results to %r.", self.survey.name, email)

        self.request.session.pop(self.get_session_key())
        return HttpResponseRedirect(reverse("survey:survey-submitted", kwargs=self.kwargs))

    def form_invalid(self, **kwargs: Any) -> HttpResponse:
        """Handle invalid form."""
        return self.render_to_response(self.get_context_data(**kwargs))


class SurveyEnterView(FormView):
    """Request a key to enter the survey."""

    survey: Survey
    template_name = "survey/survey_enter.html"
    form_class = SurveyEnterForm

    def dispatch(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        """Dispatch HTTP request."""
        self.survey = cast(
            Survey, get_object_or_404(Survey.filter_open(Survey.objects.all()), slug=self.kwargs["slug"])
        )
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Get template context data."""
        context: dict[str, Any] = super().get_context_data(**kwargs)
        context["survey"] = self.survey
        return context

    def form_valid(self, form: SurveyEnterForm) -> HttpResponse:
        """Handle valid form."""
        self.success_url = reverse(
            "survey:survey-detail",
            kwargs={
                "slug": self.kwargs["slug"],
                "code": utils.create_survey_code(form.cleaned_data["key"]),
            },
        )
        return super().form_valid(form)


class SurveySubmittedView(TemplateView):
    """Render thank you page."""

    survey: Survey
    template_name = "survey/survey_submitted.html"

    def get(self, request: HttpRequest, *args: Any, **kwargs: Any) -> HttpResponse:
        """Handle GET request."""
        self.survey = cast(
            Survey, get_object_or_404(Survey.filter_open(Survey.objects.all()), slug=self.kwargs["slug"])
        )

        if not self.survey.responses.filter(code=self.kwargs["code"].lower()).exists():
            return HttpResponseRedirect(reverse("survey:survey-detail", kwargs=self.kwargs))

        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs: Any) -> dict[str, Any]:
        """Get template context data."""
        context: dict[str, Any] = super().get_context_data(**kwargs)
        context["survey"] = self.survey
        return context
