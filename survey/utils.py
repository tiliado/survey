"""Various utility functions."""

import binascii
import re
from hashlib import sha256
from typing import Optional

SURVEY_KEY_REGEX = re.compile(r"^[-._\s]*[a-zA-Z0-9][-._\sa-zA-Z0-9]*$")  # noqa: SC300
NOT_APLHANUMERIC_REGEX = re.compile(r"[\W_]+", re.ASCII)


def normalize_survey_code(code: Optional[str]) -> str:
    """Return normalized code or raise ValueError if the code is invalid."""
    if code is not None:
        code = code.strip()

    if not code:
        raise ValueError("Code is empty")

    try:
        b = binascii.unhexlify(code.encode("ascii"))
    except (TypeError, binascii.Error, UnicodeEncodeError) as e:
        raise ValueError(f"Failed to decode code hash {code!r}: {e}")

    if len(b) != 32:  # sha256 is 32 bytes
        raise ValueError(f"Invalid length of hash {code!r}: {len(b)}")

    return code.lower()


def normalize_survey_key(key: Optional[str]) -> str:
    """Normalize survey key or raise ValueError if key is empty or invalid."""
    if key is not None:
        key = key.strip()
    if not key:
        raise ValueError("Key is empty")

    m = SURVEY_KEY_REGEX.match(key)
    if m is None:
        raise ValueError("Key is invalid")

    return extract_aplhanumeric(key).lower()


def extract_aplhanumeric(s: str) -> str:
    """Strip non-alphanumeric characters."""
    return NOT_APLHANUMERIC_REGEX.sub("", s)


def create_survey_code(key: Optional[str]) -> str:
    """Create survey code from survey key."""
    return sha256(normalize_survey_key(key).encode("ascii")).hexdigest()


def markdownify_links_target_blank(attrs: dict, new: bool = False) -> dict:
    """Add target blank for external links."""
    if attrs[(None, "href")].startswith(("http://", "https://")):
        attrs[(None, "target")] = "_blank"
    return attrs
