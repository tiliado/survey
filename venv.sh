#!/bin/bash
APP=survey
export VENV_ROOT=~/.virtualenvs
export VENV_PATH=${VENV_ROOT}/${APP}
export DJANGO_SETTINGS_MODULE="local_cfg.settings"
make venv
. ${VENV_PATH}/bin/activate
export PYTHONPATH="$PWD:$PYTHONPATH"
